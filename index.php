<?php
include 'koneksi.php';
session_start();
if(isset($_SESSION['login_user'])){
  header ('location:admin/beranda.php');
}
?>

<!DOCTYPE html>
<html>
 <head>
   <title>INVENTARISIR SKANIC</title>

      <!-- Bootstrap -->
      <link rel="stylesheet" href="assets/css/bootstrap.css"/>
      <link rel="stylesheet" href="assets/css/font-awesome.css"/>
      <link href='assets/img/sknc.png' rel='shortcut icon'>
      <!-- Java Script -->
      <script type="text/javascript" src="assets/js/jquery-2.1.4.js"></script>
      <script type="text/javascript" src="assets/js/bootstrap.js"></script>

      <style>
      body{
      margin:0;
      }
      .jumbotron {
      background: url(assets/img/lepi.png);
      background-size: 100%;
      background-repeat: repeat-x;
      color: #fff;
      padding: 150px 25px;
      height: 400px;
      }
      .navbar-default {
        background: #2980b9;
      }
      .no-border {
        border: 0;
      }
      .no-radius {
        border-radius: 0px;
      }
      .circle {
        border-radius: 50%;
      }
      .shadow {
        box-shadow: 3px 1px 2px 2px rgba(0, 0, 0, 0.2);
      }
      .navbar-default .navbar-brand {
        color: white;
        font-size: 20px;
        letter-spacing: 2px;
      }
      .navbar-default .navbar-nav > li > a {
        color: white;
      }
      .navbar-default .navbar-nav > li > a:hover {
        color: #2980b9;
        background: white;
      }
      .navbar-default .navbar-nav > .open > a, .navbar-default .navbar-nav > .open > a:hover, .navbar-default .navbar-nav > .open > a:focus {
        color: #2980b9;
        background: white;
      }
      .navbar-default .navbar-brand:hover, .navbar-default .navbar-brand:focus {
      color: #fff;
      background-color: transparent;
      }
      .dropdown-menu > li > a {
        color : #2980b9;
      }
      .container-fluid {
      padding: 60px 50px;
      }
      .bg-grey {
      background-color: #f6f6f6;
      }
      .logo-small {
      color: #fff;
      font-size: 50px;
      }
      .logo {
      color: #3498db;
      font-size: 200px;
      }
      @media screen and (max-width: 768px) {
      .col-sm-4 {
      text-align: center;
      margin: 25px 0;
      }
      }
      </style>
 </head>
 <body id="myPage" data-spy="scroll" data-target=".navbar" data-offset="60">

   <!-- Static navbar -->
    <nav class="navbar navbar-default navbar-fixed-top no-border no-radius shadow">
      <div class="container">
        <div class="navbar-header">
          <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
            <span class="sr-only">Toggle navigation</span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
            <span class="icon-bar"></span>
          </button>
          <a class="navbar-brand" href="#" style="margin-top: -3px;">INVENTARISIR <span class="glyphicon glyphicon-edit logo-small" style="font-size: 25px;"></span></a>
        </div>
        <div id="navbar" class="navbar-collapse collapse">
          <ul class="nav navbar-nav navbar-right">
            <li class="dropdown">
                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-haspopup="true" aria-expanded="false">Login <span class="caret"></span></a>
                <ul class="dropdown-menu">
                  <li><a href="admin/login.php">Admin</a></li>
                </ul>
              </li>
          </ul>
        </div><!--/.nav-collapse -->
      </div><!--/.container-fluid -->
    </nav>

   <!-- Jumbotron -->
   <div class="">
  <div class="jumbotron">
    <h1>Aplikasi Peminjaman Barang LAB Komputer</h1>
    <p>SMK Negeri 1 Ciomas</p>
  </div>
</div>
  <!-- Container (About Section) -->
  <div class="container-fluid">
    <div class="row">
      <div class="col-sm-8">
        <h2>Tentang Inventarisir</h2>
        <p>Aplikasi Peminjaman Barang LAB Komputer yang di singkat INVENTARISIR, merupakan suatu aplikasi berbasis web. Aplikasi ini di buat dengan tujuan untuk mendata barang yang tersedia agar lebih akurat karena merupakan aset yang dimiliki oleh sekolah. Serta mempermudah proses peminjaman barang yang ada di LAB komputer SMK Negeri 1 Ciomas.<br>
        Dengan demikian, di harapkan aplikasi ini dapat bermanfaat bagi sekolah.</p>
      </div>
      <div class="col-sm-4">
        <span class="glyphicon glyphicon-edit logo"></span>
      </div>
    </div>
  </div>

  <!-- Footer -->
  <div class="panel-footer">
  Copyright 2019 SMKN1CIOMAS
  <br>
  </div>

 </body>
</html>
