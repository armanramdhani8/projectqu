<?php

?>
<!DOCTYPE html>
<html>
  <head>
    <title>Login</title>

      <!-- Bootstrap -->
      <link rel="stylesheet" href="assets/css/bootstrap.css"/>
      <link rel="stylesheet" href="assets/css/font-awesome.css"/>
      <!-- CSS -->
      <link rel="stylesheet" href="assets/css/form-elements.css">
      <link href='assets/img/icon.png' rel='shortcut icon'>
      <!--Script Javascript-->
      <script type="text/javascript" src="assets/js/jquery-2.1.4.js"></script>
      <script src="assets/js/jquery.backstretch.min.js"></script>
      <script type="text/javascript" src="assets/js/bootstrap.js"></script>
      <style>
      body{
      margin:0;
      }
      .jumbotron {
      background-color: #3498db;
      color: #fff;
      padding: 100px 25px;
      }
      .navbar {
      margin-bottom: 0;
      background-color: #3498db;
      z-index: 9999;
      border: 0;
      font-size: 12px !important;
      line-height: 1.42857143 !important;
      letter-spacing: 4px;
      border-radius: 0;
      }
      .navbar li a, .navbar .navbar-brand {
      color: #fff !important;
      }
      .navbar-nav li a:hover, .navbar-nav li.active a {
      color: #3498db !important;
      background-color: #fff !important;
      }
      .navbar-default .navbar-toggle {
      border-color: transparent;
      color: #fff !important;
      }
      .container-fluid {
      padding: 60px 50px;
      }
      </style>
  </head>
  <body>

    <!-- Navbar -->
    <nav class="navbar navbar-default navbar-fixed-top">
      <div class="container">
        <div class="collapse navbar-collapse" id="myNavbar">
          <div class="navbar-header">
           <a class="navbar-brand" href="#">APEL</a>
         </div>
        </div>
      </div>
    </nav>

     <!-- Jumbotron -->
    <div class="jumbotron text-center">
     <h1>PEMINJAMAN BARANG LAB KOMPUTER</h1>
       <h2>SMK NEGERI 1 CIOMAS</h2>
    </div>

    <div class="row">
      <div class="col-sm-6"><a href="admin/login.php" class="btn btn-primary btn-block btn-lg">Admin</a></div>
      <div class="col-sm-6"><a href="kaprog/login.php" class="btn btn-primary btn-block btn-lg">Kepala Program</a></div>
    </div>

  </body>
</html>
