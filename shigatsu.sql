-- phpMyAdmin SQL Dump
-- version 4.2.11
-- http://www.phpmyadmin.net
--
-- Host: 127.0.0.1
-- Generation Time: Jun 08, 2017 at 08:58 AM
-- Server version: 5.6.21
-- PHP Version: 5.6.3

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;

--
-- Database: `shigatsu`
--

-- --------------------------------------------------------

--
-- Table structure for table `tbl_barang`
--

CREATE TABLE IF NOT EXISTS `tbl_barang` (
`id_barang` int(11) NOT NULL,
  `id_jenis_barang` int(11) NOT NULL,
  `nama_barang` varchar(100) DEFAULT NULL,
  `merk` varchar(50) DEFAULT NULL,
  `kondisi` varchar(20) DEFAULT NULL,
  `tanggal` varchar(30) DEFAULT NULL,
  `level_barang` varchar(20) DEFAULT NULL,
  `keterangan` varchar(500) DEFAULT NULL,
  `gambar` varchar(500) DEFAULT NULL,
  `status_barang` int(1) DEFAULT '0'
) ENGINE=InnoDB AUTO_INCREMENT=55 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_barang`
--

INSERT INTO `tbl_barang` (`id_barang`, `id_jenis_barang`, `nama_barang`, `merk`, `kondisi`, `tanggal`, `level_barang`, `keterangan`, `gambar`, `status_barang`) VALUES
(36, 1, 'acer 001', 'acer aspire e1-471', 'Baik', '07-06-2017', NULL, '-', 'laptop2.jpg', 1),
(38, 1, 'acer 002', 'acer aspire e1-471', 'Baik', '07-06-2017', NULL, '-', 'HDMI Cable Filled-100.png', 1),
(39, 1, 'acer 003', 'acer aspire e1-471', 'Rusak', '07-06-2017', NULL, '-', '', 1),
(40, 1, 'acer 004', 'acer aspire e1-471', 'Baik', '07-06-2017', NULL, '-', 'Generic Mouse-96.png', 0),
(41, 1, 'acer 005', 'acer aspire e1-471', 'Rusak', '07-06-2017', NULL, '-', '', 0),
(42, 1, 'lenovo 001', 'lenovo idea pad 100', 'Baik', '07-06-2017', NULL, '-', 'lepi.png', 0),
(43, 1, 'lenovo 002', 'lenovo idea pad 100', 'Baik', '07-06-2017', NULL, '-', 'HDMI Cable Filled-100.png', 0),
(44, 1, 'lenovo 003', 'lenovo idea pad 100', 'Baik', '07-06-2017', NULL, '-', 'HDMI Cable Filled-100.png', 0),
(45, 1, 'lenovo 004', 'lenovo idea pad 100', 'Baik', '07-06-2017', NULL, '-', 'HDMI Cable Filled-100.png', 0),
(46, 1, 'lenovo 005', 'lenovo idea pad 100', 'Baik', '07-06-2017', NULL, '-', 'HDMI Cable Filled-100.png', 0),
(47, 1, 'lenovo 001', 'lenovo b40', 'Baik', '07-06-2017', NULL, '-', 'HDMI Cable Filled-100.png', 1),
(48, 1, 'lenovo 002', 'lenovo b40', 'Baik', '07-06-2017', NULL, '-', 'HDMI Cable Filled-100.png', 0),
(49, 1, 'lenovo 003', 'lenovo b40', 'Baik', '07-06-2017', NULL, '-', 'HDMI Cable Filled-100.png', 0),
(50, 1, 'hp 001', 'hp', 'Baik', '07-06-2017', NULL, '-', 'HDMI Cable Filled-100.png', 0),
(51, 1, 'hp 002', 'hp', 'Baik', '07-06-2017', NULL, '-', 'CD-100.png', 0),
(52, 3, 'cd 001', 'Cd Ukom 15/16', 'Baik', '07-06-2017', NULL, '-', 'CD-100.png', 0),
(53, 3, 'cd 002', 'Cd ukom 14/15', 'Baik', '07-06-2017', NULL, '-', 'CD-100.png', 0),
(54, 3, 'cd 003', 'Cd ukom 13/14', 'Baik', '07-06-2017', NULL, '-', 'CD-100.png', 0);

-- --------------------------------------------------------

--
-- Table structure for table `tbl_detail_pinjam`
--

CREATE TABLE IF NOT EXISTS `tbl_detail_pinjam` (
`id_detail_pinjam` int(11) NOT NULL,
  `id_pinjam` int(11) DEFAULT NULL,
  `id_barang` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=latin1;

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenis_barang`
--

CREATE TABLE IF NOT EXISTS `tbl_jenis_barang` (
`id_jenis_barang` int(11) NOT NULL,
  `nama_jenis` varchar(50) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=28 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jenis_barang`
--

INSERT INTO `tbl_jenis_barang` (`id_jenis_barang`, `nama_jenis`) VALUES
(1, 'Laptop'),
(2, 'Headset'),
(3, 'CD'),
(4, 'Port Lan'),
(5, 'Harddisk'),
(6, 'Barcode'),
(7, 'WPS'),
(8, 'Modem'),
(9, 'Obeng'),
(10, 'Kabel'),
(11, 'Remote speaker'),
(12, 'Router+kabel power'),
(13, 'Ram'),
(14, 'Keyboard'),
(16, 'Speaker Mini'),
(17, 'Mouse'),
(18, 'Dvd Rom'),
(19, 'Baterai Al Kaline'),
(20, 'Lan'),
(21, 'Heat Sink'),
(22, 'Cover CD'),
(23, 'RJ-45'),
(24, 'Suntikan Toner'),
(25, 'VGA Card'),
(26, 'Processor'),
(27, 'Cutter Kabel  Lan');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_jenis_peminjam`
--

CREATE TABLE IF NOT EXISTS `tbl_jenis_peminjam` (
`id_jenis_peminjam` int(11) NOT NULL,
  `jenis_peminjam` varchar(30) DEFAULT NULL
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_jenis_peminjam`
--

INSERT INTO `tbl_jenis_peminjam` (`id_jenis_peminjam`, `jenis_peminjam`) VALUES
(1, 'siswa'),
(2, 'guru'),
(3, 'tu');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pegawai`
--

CREATE TABLE IF NOT EXISTS `tbl_pegawai` (
`id_pegawai` int(11) NOT NULL,
  `nip` varchar(30) NOT NULL,
  `nama` varchar(50) NOT NULL,
  `jenis_kelamin` varchar(2) NOT NULL,
  `jabatan` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=45 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pegawai`
--

INSERT INTO `tbl_pegawai` (`id_pegawai`, `nip`, `nama`, `jenis_kelamin`, `jabatan`) VALUES
(1, '19670601 200003 1 003', 'Drs.Miswan Wahyudi, MM', 'L', 'Kepala Sekolah'),
(2, '19730707 200604 1 013', 'Hajarol Harahap, S.Pd', 'L', 'Guru'),
(3, '19801203 200902 2 001', 'Lina Yudiastuti, S.Pd,Si', 'P', 'Guru'),
(4, '19770728 201001 1 002', 'Bachtiar, S.Pd', 'L', 'Guru'),
(5, '19780822 201001 1 013', 'Salikin, S.Pd', 'L', 'Guru'),
(6, '19820223 201001 1 003', 'Kartanto, S.Pd', 'L', 'Guru'),
(7, '19800905 201101 2 001', 'Maesaroh, S.PdI', 'P', 'Guru'),
(8, '19820725 201101 2 003', 'Mardiana Palantika, S.Pd', 'P', 'Guru'),
(9, '19880130 201101 1 001', 'Januar Ashari, S.Pd', 'L', 'Guru'),
(10, '19870320 201501 1 002', 'Ajid, S.Ds', 'L', 'Guru'),
(11, '-', 'Zulkarnaen Noor Syarief, S. Kom', 'L', 'Guru Honor'),
(12, '19610528 198903 2 001', 'Dra. Pratiwi Nurhayati', 'P', 'Guru'),
(13, '-', 'Dwijo Riyanto Wibowo, S.Kom', 'L', 'Guru Honor'),
(14, '-', 'Nana Setiana, S.P', 'L', 'Guru Honor'),
(15, '-', 'Erwan Usmawan, SST', 'L', 'Guru Honor'),
(16, '-', 'Nidia Desiyanti, S.Pd', 'P', 'Guru Honor'),
(17, '-', 'Hasrul Adiputra Harahap, S.Kom', 'L', 'Guru Honor'),
(18, '-', 'Zulfikar', 'L', 'Guru Honor'),
(19, '-', 'Wanda Kurniawan', 'L', 'Guru Honor'),
(20, '-', 'Alif Ruhbi', 'L', 'Guru Honor'),
(21, '-', 'Ahmad Faisol H,S.Pd', 'L', 'Guru Honor'),
(22, '-', 'Siti Nurhayatulismah, S.Pd', 'P', 'Guru Honor'),
(23, '19611024 198902 2 001', 'Dra. Susetyawati', 'P', 'Guru'),
(24, '-', 'Abdul Ruhyat, S.Pdi', 'L', 'Guru Honor'),
(25, '-', 'Lutfi Choeril, S.Pd', 'L', 'Guru Honor'),
(26, '-', 'Muhammad Ridwan', 'L', 'Pegawai'),
(27, '-', 'Boma Bondan Suharto, S.Sos', 'L', 'Guru Honor'),
(28, '-', 'Rini Kusniati, S.Pd', 'P', 'Guru Honor'),
(29, '-', 'Wita Yulistia, S.Pd', 'P', 'Guru Honor'),
(30, '-', 'Riyana Hermadiyana, S.Si', 'P', 'Guru Honor'),
(31, '-', 'Alex Abules, S.Pd', 'L', 'Guru Honor'),
(32, '-', 'Tedi Hariadi, S.Kom', 'L', 'Guru Honor'),
(33, '-', 'Sri Mulyati, S.Pd', 'P', 'Guru Honor'),
(34, '-', 'Nani Maryani, S.Pd', 'P', 'Guru Honor'),
(35, '19630912 198501 2 001', 'Hj. Elismawati, S.Pd', 'P', 'Guru'),
(36, '-', 'Runen', 'L', 'Guru Honor'),
(37, '-', 'Sofian', 'L', 'Guru Honor'),
(38, '-', 'Heru Setiawan, S.Kom', 'L', 'Guru Honor'),
(39, '-', 'Aviani Sofiansyah', 'L', 'Guru Honor'),
(40, '19661104 199512 2 002', 'Lina Deliana, S.Pd', 'P', 'Guru'),
(41, '19660215 199702 2 001', 'Dra. Siwi Purwantini', 'P', 'Guru'),
(42, '19660805 199802 1 002', 'Drs. Kurnadi', 'L', 'Guru'),
(43, '19691024 200312 2 001', 'Hj. Neti Risnawati, M.Pd', 'P', 'Guru'),
(44, '19690913 200312 1 003', 'Dono Wasito, S.Pd', 'L', 'Guru');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_pinjam`
--

CREATE TABLE IF NOT EXISTS `tbl_pinjam` (
`id_pinjam` int(11) NOT NULL,
  `id_peminjam` int(11) NOT NULL,
  `id_jenis_barang` int(11) NOT NULL,
  `id_barang` int(11) DEFAULT NULL,
  `id_jenis_peminjam` int(11) DEFAULT NULL,
  `jumlah_pinjam` varchar(10) NOT NULL,
  `waktu_pinjam` varchar(30) NOT NULL,
  `waktu_kembali` varchar(30) NOT NULL,
  `catatan` varchar(50) NOT NULL,
  `status_pinjam` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=53 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_pinjam`
--

INSERT INTO `tbl_pinjam` (`id_pinjam`, `id_peminjam`, `id_jenis_barang`, `id_barang`, `id_jenis_peminjam`, `jumlah_pinjam`, `waktu_pinjam`, `waktu_kembali`, `catatan`, `status_pinjam`) VALUES
(46, 189, 1, 36, 1, '1', '07-06-2017', '', '-', '1'),
(47, 10, 1, 36, 2, '1', '07-06-2017', '', '-', '1'),
(49, 3, 1, 38, 2, '1', '07-06-2017', '', '-', '0'),
(50, 65, 1, 47, 1, '1', '07-06-2017', '', '-', '0'),
(51, 4, 1, 36, 2, '1', '08-06-2017', '', '-', '0'),
(52, 2, 1, 39, 1, '1', '08-06-2017', '', '-', '0');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_siswa`
--

CREATE TABLE IF NOT EXISTS `tbl_siswa` (
`id_siswa` int(11) NOT NULL,
  `nis` varchar(30) NOT NULL,
  `nisn` varchar(30) NOT NULL,
  `nama` varchar(60) NOT NULL,
  `kelas` varchar(30) NOT NULL,
  `jenis_kelamin` varchar(30) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=218 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_siswa`
--

INSERT INTO `tbl_siswa` (`id_siswa`, `nis`, `nisn`, `nama`, `kelas`, `jenis_kelamin`) VALUES
(1, '1516070071.0', '0002551267', 'Agnia Maulida Jayusman', 'xi rpl 1', 'P'),
(2, '1516070072.0', '0002552321', 'Ahmad Maulana Yusup', 'xi rpl 1', 'L'),
(3, '1516070073.0', '0002551271', 'Aldi Hermawan', 'xi rpl 1', 'L'),
(4, '1516070074.0', '9993530406', 'Asep Zaelani', 'xi rpl 1', 'L'),
(5, '1516070075.0', '0006532168', 'Aulia Zuleikha', 'xi rpl 1', 'P'),
(6, '1516070076.0', '0002597859', 'Diah Anissa', 'xi rpl 1', 'P'),
(7, '1516070077.0', '0002550744', 'Erika Nissa Amalia', 'xi rpl 1', 'P'),
(8, '1516070078.0', '0002598165', 'Fajar Ilyasa', 'xi rpl 1', 'L'),
(9, '1516070079.0', '9996600128', 'Ghaffaridzan Raudha', 'xi rpl 1', 'L'),
(10, '1516070080.0', '9995134407', 'Hafiz Ramadhan', 'xi rpl 1', 'L'),
(11, '1516070081.0', '0002551170', 'Indah Nur''Aini ', 'xi rpl 1', 'P'),
(12, '1516070082.0', '9993636295', 'Isma Sekar Ningsih', 'xi rpl 1', 'P'),
(13, '1516070083.0', '0002597863', 'Laras Purnamasari', 'xi rpl 1', 'P'),
(14, '1516070084.0', '0003130530', 'Mela Ekatiyana', 'xi rpl 1', 'P'),
(15, '1516070085.0', '0002955636', 'Muhamad Andreansyah', 'xi rpl 1', 'L'),
(16, '1516070086.0', '0006478371', 'Muhamad Fajar Puji Tri Handoko', 'xi rpl 1', 'L'),
(17, '1516070087.0', '0006913179', 'Muhamad Rifqi Hidayat', 'xi rpl 1', 'L'),
(18, '1516070088.0', '9993638881', 'Muhammad Ali Firmansyah', 'xi rpl 1', 'L'),
(19, '1516070089.0', '0002954742', 'Muhammad Awalludin', 'xi rpl 1', 'L'),
(20, '1516070090.0', '9998115267', 'Muhammad Ichsan. F', 'xi rpl 1', 'L'),
(21, '1516070091.0', '9993636235', 'Muhammad Teguh Saputra', 'xi rpl 1', 'L'),
(22, '1516070092.0', '0002598198', 'Novi Yulianti', 'xi rpl 1', 'P'),
(23, '1516070093.0', '0002553132', 'Putri Selviati', 'xi rpl 1', 'P'),
(24, '1516070094.0', '9994176722', 'Ramdani', 'xi rpl 1', 'L'),
(25, '1516070095.0', '9993636305', 'Riska Tri Mulya Sari', 'xi rpl 1', 'P'),
(26, '1516070096.0', '9994894777', 'Rizal Anzali', 'xi rpl 1', 'L'),
(27, '1516070097.0', '0002550766', 'Selina Nabillah', 'xi rpl 1', 'P'),
(28, '1516070098.0', '9993530989', 'Siti Melania', 'xi rpl 1', 'P'),
(29, '1516070099.0', '0006891910', 'Sonya', 'xi rpl 1', 'P'),
(30, '1516070100.0', '0002539320', 'Tasya Melati Dewi', 'xi rpl 1', 'P'),
(31, '1516070101.0', '0002553145', 'Tendy Ardiansyah', 'xi rpl 1', 'L'),
(32, '1516070102.0', '0002432714', 'Vina Fitria', 'xi rpl 1', 'P'),
(33, '1516070103.0', '0010231057', 'Windi Yulianti', 'xi rpl 1', 'P'),
(34, '1516070104.0', '0001006367', 'Zulfikar Kurniawan', 'xi rpl 1', 'L'),
(35, '1516070105.0', '0008497782', 'Ahmad Diva', 'xi rpl 2', 'L'),
(36, '1516070106.0', '0002473563', 'Ahmad Mulqi', 'xi rpl 2', 'L'),
(37, '1516070108.0', '0003613975', 'Andini Putri', 'xi rpl 2', 'P'),
(38, '1516070109.0', '9993711933', 'Ayu Wandira', 'xi rpl 2', 'P'),
(39, '1516070110.0', '0006970602', 'Dedy Koeswara Ibrahim', 'xi rpl 2', 'L'),
(40, '1516070111.0', '0002552337', 'Dina Dewi Anjani', 'xi rpl 2', 'P'),
(41, '1516070112.0', '0002553159', 'Fajar Setiawan', 'xi rpl 2', 'L'),
(42, '1516070113.0', '0010231083', 'Febrilia Syahputri', 'xi rpl 2', 'P'),
(43, '1516070114.0', '9969197522', 'Gilang Ramadhan ', 'xi rpl 2', 'L'),
(44, '1516070115.0', '9993636721', 'Ikbal Ramadan', 'xi rpl 2', 'L'),
(45, '1516070116.0', '0002472592', 'Indri Handayani', 'xi rpl 2', 'P'),
(46, '1516070117.0', '0003033077', 'Kenanga Raraayunita Harianja', 'xi rpl 2', 'P'),
(47, '1516070118.0', '0004423027', 'Lela Juleha', 'xi rpl 2', 'P'),
(48, '1516070119.0', '9998117059', 'Melenia Dwi Absari', 'xi rpl 2', 'P'),
(49, '1516070120.0', '0002551300', 'Muhamad Arif', 'xi rpl 2', 'L'),
(50, '1516070121.0', '9994086863', 'Muhamad Firdaus', 'xi rpl 2', 'L'),
(51, '1516070122.0', '0002971847', 'Muhammad Abu Rizal', 'xi rpl 2', 'L'),
(52, '1516070123.0', '9998172192', 'Muhammad Alwy', 'xi rpl 2', 'L'),
(53, '1516070124.0', '0005001818', 'Muhammad Farhan Ramdani', 'xi rpl 2', 'L'),
(54, '1516070125.0', '9993636299', 'Muhammad Irfan Maulana', 'xi rpl 2', 'L'),
(55, '1516070126.0', '0006913546', 'Muhammad Zaenul Abidin', 'xi rpl 2', 'L'),
(56, '1516070127.0', '9993637001', 'Nurhayati', 'xi rpl 2', 'P'),
(57, '1516070128.0', '9993537114', 'Putri Yuliandari', 'xi rpl 2', 'P'),
(58, '1516070129.0', '0010710213', 'Reihan Nurzehan Sofyan', 'xi rpl 2', 'L'),
(59, '1516070130.0', '0002553137', 'Salsabilla Latifah', 'xi rpl 2', 'P'),
(60, '1516070131.0', '0002954861', 'Sheva Dyanza', 'xi rpl 2', 'P'),
(61, '1516070132.0', '9993519767', 'Sihabudin Azilan', 'xi rpl 2', 'L'),
(62, '1516070133.0', '0002550595', 'Siti Nurhikmah', 'xi rpl 2', 'P'),
(63, '1516070134.0', '0002599061', 'Sri Endayati', 'xi rpl 2', 'P'),
(64, '1516070135.0', '0002598217', 'Tiara Agus Triani', 'xi rpl 2', 'P'),
(65, '1516070136.0', '0002553147', 'Vini Mulyani', 'xi rpl 2', 'P'),
(66, '1516070138.0', '0002552241', 'Wulan Sari', 'xi rpl 2', 'P'),
(67, '1516070139.0', '0002842537', 'Yajid Hanafiah', 'xi rpl 2', 'L'),
(68, '1516070140.0', '0002539905', 'Zulkifli', 'xi rpl 2', 'L'),
(69, '1516070141.0', '9990962967', 'Adelia Damayanti', 'xi rpl 3', 'P'),
(70, '1516070142.0', '0002452659', 'Ahmad Fajar Maulana', 'xi rpl 3', 'L'),
(71, '1516070143.0', '0010038556', 'Ahmad Sopian', 'xi rpl 3', 'L'),
(72, '1516070144.0', '0003033198', 'Anjani Mega Siyafitri', 'xi rpl 3', 'P'),
(73, '1516070145.0', '9997292827', 'Arian Perdana', 'xi rpl 3', 'L'),
(74, '1516070146.0', '0006913972', 'Bunga Febri Aswari', 'xi rpl 3', 'P'),
(75, '1516070147.0', '0006532161', 'Dimas Anda Pringga', 'xi rpl 3', 'L'),
(76, '1516070148.0', '0002552482', 'Dita Atamia', 'xi rpl 3', 'P'),
(77, '1516070149.0', '0002598172', 'Gema Pratama Hekmatyar', 'xi rpl 3', 'L'),
(78, '1516070150.0', '9991864275', 'Hadi Aristiawan', 'xi rpl 3', 'L'),
(79, '1516070151.0', '9983492292', 'Ika Kartika Sari', 'xi rpl 3', 'P'),
(80, '1516070152.0', '0002788644', 'Iman Ahmad Bahtiar', 'xi rpl 3', 'L'),
(81, '1516070153.0', '0006913977', 'Intan Rosita', 'xi rpl 3', 'P'),
(82, '1516070154.0', '0002553114', 'Kiki Aprillia Listiani', 'xi rpl 3', 'P'),
(83, '1516070155.0', '0009570210', 'Lia Herlina ', 'xi rpl 3', 'P'),
(84, '1516070156.0', '0003033606', 'Muhamad Fadlan', 'xi rpl 3', 'L'),
(85, '1516070157.0', '0002551901', 'Muhamad Ridwan', 'xi rpl 3', 'L'),
(86, '1516070158.0', '0009552669', 'Muhammad Aditiya', 'xi rpl 3', 'L'),
(87, '1516070159.0', '0002551337', 'Muhammad Amalludin', 'xi rpl 3', 'L'),
(88, '1516070160.0', '0002598192', 'Muhammad Ghifari Arfananda', 'xi rpl 3', 'L'),
(89, '1516070161.0', '9994093473', 'Muhammad Nur Firdaus', 'xi rpl 3', 'L'),
(90, '1516070162.0', '9993639962', 'Novi Herliyanti', 'xi rpl 3', 'P'),
(91, '1516070163.0', '0002597890', 'Nurmalasari', 'xi rpl 3', 'P'),
(92, '1516070164.0', '9994093412', 'Ramadhika Saputra', 'xi rpl 3', 'L'),
(93, '1516070165.0', '0003031712', 'Reni Yuliani', 'xi rpl 3', 'P'),
(94, '1516070166.0', '0002598208', 'Reynaldy', 'xi rpl 3', 'L'),
(95, '1516070167.0', '0006912992', 'Sarah Rabila', 'xi rpl 3', 'P'),
(96, '1516070168.0', '0010230945', 'Sinta Lestari', 'xi rpl 3', 'P'),
(97, '1516070169.0', '0010236273', 'Siti Nurholipah', 'xi rpl 3', 'P'),
(98, '1516070170.0', '2539784', 'Sri Rahmah', 'xi rpl 3', 'P'),
(99, '1516070171.0', '121307116', 'Sudrazat', 'xi rpl 3', 'L'),
(100, '1516070172.0', '0006913998', 'Urmila Listiani', 'xi rpl 3', 'P'),
(101, '1516070173.0', '0014951501', 'Widya Hamidah', 'xi rpl 3', 'P'),
(102, '1516070174.0', '0002551909', 'Yogi Andrian', 'xi rpl 3', 'L'),
(103, '16170809001.0', '0017171804', 'Annisa Agustiani', 'x rpl 1', 'P'),
(104, '16170809002.0', '0010167252', 'Ahmad Setiyawan', 'x rpl 1', 'L'),
(105, '16170809003.0', '0013574024', 'Ahmad Fhiraz Darmawan Al Ghifari', 'x rpl 1', 'L'),
(106, '16170809004.0', '0003044960', 'Budi', 'x rpl 1', 'L'),
(107, '16170809005.0', '0015576134', 'Dimas Pratama', 'x rpl 1', 'L'),
(108, '16170809006.0', '0014519691', 'Davi Perdiansyah', 'x rpl 1', 'P'),
(109, '16170809007.0', '0005893950', 'Dafa Dairobi', 'x rpl 1', 'L'),
(110, '16170809008.0', '', 'Erlian Alsyaviyanti ', 'x rpl 1', 'P'),
(111, '16170809009.0', '0011141014', 'Fani Fadilah Matondang', 'x rpl 1', 'P'),
(112, '16170809010.0', '', 'Furqon', 'x rpl 1', 'L'),
(113, '16170809011.0', '0018635745', 'Harsa Aditya', 'x rpl 1', 'L'),
(114, '16170809012.0', '0013575508', 'Imanudin Elvan Haz', 'x rpl 1', 'L'),
(115, '16170809013.0', '', 'Kevine Atami Tanos', 'x rpl 1', 'L'),
(116, '16170809014.0', '0004997804', 'Lia Rukmanah', 'x rpl 1', 'P'),
(117, '16170809015.0', '0002324866', 'Muhammad Ridwan Edi Pamungkas', 'x rpl 1', 'L'),
(118, '16170809016.0', '0018593147', 'Muhamad Ade Rohayat', 'x rpl 1', 'L'),
(119, '16170809017.0', '0029223755', 'Muhammad Rafli Al Hafizh', 'x rpl 1', 'L'),
(120, '16170809018.0', '0013691189', 'Muhamad Rifki Fathukalam', 'x rpl 1', 'L'),
(121, '16170809019.0', '0018716719', 'M Andreansyah', 'x rpl 1', 'L'),
(122, '16170809020.0', '0024786639', 'M.Raffy Nurrochman', 'x rpl 1', 'L'),
(123, '16170809021.0', '', 'Muhammad Rifky Fauzan', 'x rpl 1', 'L'),
(124, '16170809022.0', '0018337198', 'Maulana Fatullah', 'x rpl 1', 'L'),
(125, '16170809023.0', '0013610774', 'Nava Faradila', 'x rpl 1', 'P'),
(126, '16170809024.0', '0003121901', 'Nadya Novayanti', 'x rpl 1', 'P'),
(127, '16170809025.0', '0011972392', 'Nadia Mulyani', 'x rpl 1', 'P'),
(128, '16170809026.0', '0013599727', 'Puput Handayani', 'x rpl 1', 'P'),
(129, '16170809027.0', '0011930184', 'Putri Mulyaningsih', 'x rpl 1', 'P'),
(130, '16170809028.0', '0001525816', 'Ratu Annisa Saidah', 'x rpl 1', 'P'),
(131, '16170809029.0', '', 'Ravi Rahmawansyah', 'x rpl 1', 'L'),
(132, '16170809030.0', '0025699323', 'Risa Angraeni', 'x rpl 1', 'P'),
(133, '16170809031.0', '9993638818', 'Rezki Kartika Permana', 'x rpl 1', 'P'),
(134, '16170809032.0', '0001703755', 'Raihan Nizar Jawahair', 'x rpl 1', 'L'),
(135, '16170809033.0', '-', 'Rena Triyana Ningsih', 'x rpl 1', 'P'),
(136, '16170809034.0', '', 'Siti  Aulia  Kartini', 'x rpl 1', 'P'),
(137, '16170809035.0', '0012546624', 'Siti Nurul Ardenia Ningsih Kholifah', 'x rpl 1', 'P'),
(138, '16170809036.0', '', 'Siti Rohmah', 'x rpl 1', 'P'),
(139, '16170809037.0', '0014951727', 'Siti Widiasari', 'x rpl 1', 'P'),
(140, '16170809038.0', '0015241777', 'Yulia Fauziah', 'x rpl 1', 'P'),
(141, '16170809039.0', '0017171792', 'Alief Juan Aprian Tiarsa', 'x rpl 2', 'L'),
(142, '16170809040.0', '0015390320', 'Aji Febriansyah', 'x rpl 2', 'L'),
(143, '16170809041.0', '0018033868', 'Anjas Wicaksana', 'x rpl 2', 'L'),
(144, '16170809042.0', '0018710100', 'Bagas Pratama', 'x rpl 2', 'L'),
(145, '16170809043.0', '0018710054', 'Diki Julianto', 'x rpl 2', 'L'),
(146, '16170809044.0', '0001729945', 'Deli Caesar Mahesa', 'x rpl 2', 'L'),
(147, '16170809045.0', '0021478801', 'Dava Ahmad Pratama', 'x rpl 2', 'L'),
(148, '16170809046.0', '0015017671', 'Ernandi Winata', 'x rpl 2', 'L'),
(149, '16170809047.0', '', 'Finka Aprilia', 'x rpl 2', 'P'),
(150, '16170809048.0', '0018014140', 'Firstio Daniel Anmar', 'x rpl 2', 'L'),
(151, '16170809049.0', '0006532176', 'Haniya Qothrunnada', 'x rpl 2', 'P'),
(152, '16170809050.0', '0003125140', 'Ira Yuniar', 'x rpl 2', 'P'),
(153, '16170809051.0', '0014946668', 'Lita', 'x rpl 2', 'P'),
(154, '16170809052.0', '', 'Mulyanah', 'x rpl 2', 'P'),
(155, '16170809053.0', '0011977377', 'Muhammad Ilham Maulana Pratama Darmansyah', 'x rpl 2', 'L'),
(156, '16170809054.0', '0001627830', 'M.Fikri', 'x rpl 2', 'L'),
(157, '16170809055.0', '0026051007', 'Muhamad Naufal Musyaffa', 'x rpl 2', 'L'),
(158, '16170809056.0', '0013038995', 'M Reza Aditya ', 'x rpl 2', 'L'),
(159, '16170809057.0', '0014146918', 'M. Farhan Syamsudin', 'x rpl 2', 'L'),
(160, '16170809058.0', '', 'Muchamad Arif Maulana', 'x rpl 2', 'L'),
(161, '16170809059.0', '0018255819', 'Meita Tiara Putri', 'x rpl 2', 'P'),
(162, '16170809060.0', '', 'Muhamad Wahyudin', 'x rpl 2', 'L'),
(163, '16170809061.0', '0012488010', 'Nur Fitria', 'x rpl 2', 'P'),
(164, '16170809062.0', '0001509119', 'Nurul Putri Ernawan', 'x rpl 2', 'P'),
(165, '16170809063.0', '0001045179', 'Nurul Fitriyah Ramadoni', 'x rpl 2', 'P'),
(166, '16170809064.0', '0014951679', 'Putri Nadia', 'x rpl 2', 'P'),
(167, '16170809065.0', '0011322951', 'Riskyawati', 'x rpl 2', 'P'),
(168, '16170809066.0', '0018579228', 'Resky Saepudin', 'x rpl 2', 'L'),
(169, '16170809067.0', '0014472670', 'Reyhan Hakiim', 'x rpl 2', 'L'),
(170, '16170809068.0', '0018710035', 'Reni Natasavrian', 'x rpl 2', 'P'),
(171, '16170809069.0', '0011905908', 'Rafli Rafiqasyah', 'x rpl 2', 'L'),
(172, '16170809070.0', '0011972394', 'Rama Dandiyawan', 'x rpl 2', 'L'),
(173, '16170809071.0', '0013610933', 'Siti Lastri Fauziah', 'x rpl 2', 'P'),
(174, '16170809072.0', '0003032942', 'Selvi', 'x rpl 2', 'P'),
(175, '16170809073.0', '0011978507', 'Seulgi Aulia', 'x rpl 2', 'P'),
(176, '16170809074.0', '0001045104', 'Sani Lestari ', 'x rpl 2', 'P'),
(177, '16170809075.0', '0021432996', 'Toniyansyah Wahyudi', 'x rpl 2', 'L'),
(178, '16170809076.0', '0018015589', 'Zahra Mutia Affandi', 'x rpl 2', 'P'),
(179, '16170809077.0', '0001045204', 'Heruwansyah', 'x rpl 2', 'L'),
(180, '16170809078.0', '0005894098', 'Arman Ramdhani', 'x rpl 3', 'L'),
(181, '16170809079.0', '0015312553', 'Amanda Siti Jubaedah', 'x rpl 3', 'P'),
(182, '16170809080.0', '0012357221', 'Annisa Ulhusnah', 'x rpl 3', 'P'),
(183, '16170809081.0', '0012013090', 'Dandy Junaedy', 'x rpl 3', 'L'),
(184, '16170809082.0', '0018635758', 'Dudi Iskandar', 'x rpl 3', 'L'),
(185, '16170809083.0', '0003121883', 'Dini Nursafitri', 'x rpl 3', 'P'),
(186, '16170809084.0', '0003082226', 'Erna Sulistiani', 'x rpl 3', 'P'),
(187, '16170809085.0', '', 'Elsa Noviani', 'x rpl 3', 'P'),
(188, '16170809086.0', '', 'Fathurohman Nurasyam', 'x rpl 3', 'L'),
(189, '16170809087.0', '', 'Firda Febrianti', 'x rpl 3', 'P'),
(190, '16170809088.0', '131407057', 'I Komang Chitta Tri Suyadnya', 'x rpl 3', 'L'),
(191, '16170809089.0', '', 'Indah Fitria', 'x rpl 3', 'P'),
(192, '16170809090.0', '0008597176', 'Luthfi Ramadhan', 'x rpl 3', 'L'),
(193, '16170809091.0', '', 'Muhamad Rafiq Abdul Aziz', 'x rpl 3', 'L'),
(194, '16170809092.0', '0018256887', 'Muhammad Rifa''I', 'x rpl 3', 'L'),
(195, '16170809093.0', '', 'Muhammad Jayyidan', 'x rpl 3', 'L'),
(196, '16170809094.0', '0014951973', 'Maudy Fitri Fazriani', 'x rpl 3', 'P'),
(197, '16170809095.0', '0011977384', 'Muhammad Ayis Su''Di', 'x rpl 3', 'L'),
(198, '16170809096.0', '0005893954', 'Muhammad Rifky Afrizal', 'x rpl 3', 'L'),
(199, '16170809097.0', '0011990480', 'Muhammad Feri Perdiasnyah', 'x rpl 3', 'L'),
(200, '16170809098.0', '0024794986', 'Mohamad Rahfi Fahrezi', 'x rpl 3', 'L'),
(201, '16170809099.0', '0008595959', 'Mohamad Raja Fakhri', 'x rpl 3', 'L'),
(202, '16170809100.0', '0011978474', 'Nur Hanipah', 'x rpl 3', 'P'),
(203, '16170809101.0', '', 'Nabila Silfani', 'x rpl 3', 'P'),
(204, '16170809102.0', '0004404323', 'Putri Azizah Az-Zahra', 'x rpl 3', 'P'),
(205, '16170809103.0', '0018731575', 'Puspita Wulandari', 'x rpl 3', 'P'),
(206, '16170809104.0', '0012337651', 'Riska Amalia Putri', 'x rpl 3', 'P'),
(207, '16170809105.0', '0015576156', 'Rahayu Widiyastuti', 'x rpl 3', 'P'),
(208, '16170809106.0', '000430532', 'Rizqi Fadhillah ', 'x rpl 3', 'L'),
(209, '16170809107.0', '0013574012', 'Rinaldi Ardiansyah', 'x rpl 3', 'L'),
(210, '16170809108.0', '0014951603', 'Rheina Apriliana', 'x rpl 3', 'L'),
(211, '16170809109.0', '00136100951', 'Rizki Muhtiani Putri', 'x rpl 3', 'P'),
(212, '16170809110.0', '0011972414', 'Shela Listiani', 'x rpl 3', 'P'),
(213, '16170809111.0', '', 'Silvani Arifin', 'x rpl 3', 'P'),
(214, '16170809112.0', '0018602119', 'Sri Indah Lestari', 'x rpl 3', 'P'),
(215, '16170809113.0', '', 'Siti Nurul Laela Afifah', 'x rpl 3', 'P'),
(216, '16170809114.0', '0009976361', 'Yoga Dwi Septira', 'x rpl 3', 'L'),
(217, '16170809115.0', '', 'Maudhio Andre Wijaya', 'x rpl 3', 'L');

-- --------------------------------------------------------

--
-- Table structure for table `tbl_user`
--

CREATE TABLE IF NOT EXISTS `tbl_user` (
`id_user` int(11) NOT NULL,
  `id_pegawai` int(11) NOT NULL,
  `username` varchar(32) NOT NULL,
  `password` varchar(32) NOT NULL,
  `level` varchar(15) NOT NULL
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=latin1;

--
-- Dumping data for table `tbl_user`
--

INSERT INTO `tbl_user` (`id_user`, `id_pegawai`, `username`, `password`, `level`) VALUES
(1, 32, 'admin', '21232f297a57a5a743894a0e4a801fc3', 'admin'),
(2, 24, 'kaprog', '042bf0fbde3fb2b24fc84e1d81687b26', 'kaprog');

--
-- Indexes for dumped tables
--

--
-- Indexes for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
 ADD PRIMARY KEY (`id_barang`), ADD KEY `id_jenis_barang` (`id_jenis_barang`);

--
-- Indexes for table `tbl_detail_pinjam`
--
ALTER TABLE `tbl_detail_pinjam`
 ADD PRIMARY KEY (`id_detail_pinjam`), ADD KEY `id_pinjam` (`id_pinjam`), ADD KEY `id_barang` (`id_barang`);

--
-- Indexes for table `tbl_jenis_barang`
--
ALTER TABLE `tbl_jenis_barang`
 ADD PRIMARY KEY (`id_jenis_barang`);

--
-- Indexes for table `tbl_jenis_peminjam`
--
ALTER TABLE `tbl_jenis_peminjam`
 ADD PRIMARY KEY (`id_jenis_peminjam`);

--
-- Indexes for table `tbl_pegawai`
--
ALTER TABLE `tbl_pegawai`
 ADD PRIMARY KEY (`id_pegawai`);

--
-- Indexes for table `tbl_pinjam`
--
ALTER TABLE `tbl_pinjam`
 ADD PRIMARY KEY (`id_pinjam`), ADD KEY `id_peminjam` (`id_peminjam`), ADD KEY `id_barang` (`id_jenis_barang`), ADD KEY `id_jenis_peminjam` (`id_jenis_peminjam`), ADD KEY `id_barang_2` (`id_barang`);

--
-- Indexes for table `tbl_siswa`
--
ALTER TABLE `tbl_siswa`
 ADD PRIMARY KEY (`id_siswa`);

--
-- Indexes for table `tbl_user`
--
ALTER TABLE `tbl_user`
 ADD PRIMARY KEY (`id_user`), ADD UNIQUE KEY `ktp` (`id_pegawai`);

--
-- AUTO_INCREMENT for dumped tables
--

--
-- AUTO_INCREMENT for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
MODIFY `id_barang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=55;
--
-- AUTO_INCREMENT for table `tbl_detail_pinjam`
--
ALTER TABLE `tbl_detail_pinjam`
MODIFY `id_detail_pinjam` int(11) NOT NULL AUTO_INCREMENT;
--
-- AUTO_INCREMENT for table `tbl_jenis_barang`
--
ALTER TABLE `tbl_jenis_barang`
MODIFY `id_jenis_barang` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=28;
--
-- AUTO_INCREMENT for table `tbl_jenis_peminjam`
--
ALTER TABLE `tbl_jenis_peminjam`
MODIFY `id_jenis_peminjam` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=4;
--
-- AUTO_INCREMENT for table `tbl_pegawai`
--
ALTER TABLE `tbl_pegawai`
MODIFY `id_pegawai` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=45;
--
-- AUTO_INCREMENT for table `tbl_pinjam`
--
ALTER TABLE `tbl_pinjam`
MODIFY `id_pinjam` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=53;
--
-- AUTO_INCREMENT for table `tbl_siswa`
--
ALTER TABLE `tbl_siswa`
MODIFY `id_siswa` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=218;
--
-- AUTO_INCREMENT for table `tbl_user`
--
ALTER TABLE `tbl_user`
MODIFY `id_user` int(11) NOT NULL AUTO_INCREMENT,AUTO_INCREMENT=3;
--
-- Constraints for dumped tables
--

--
-- Constraints for table `tbl_barang`
--
ALTER TABLE `tbl_barang`
ADD CONSTRAINT `tbl_barang_ibfk_1` FOREIGN KEY (`id_jenis_barang`) REFERENCES `tbl_jenis_barang` (`id_jenis_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_detail_pinjam`
--
ALTER TABLE `tbl_detail_pinjam`
ADD CONSTRAINT `tbl_detail_pinjam_ibfk_1` FOREIGN KEY (`id_pinjam`) REFERENCES `tbl_pinjam` (`id_pinjam`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tbl_detail_pinjam_ibfk_2` FOREIGN KEY (`id_barang`) REFERENCES `tbl_barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_pinjam`
--
ALTER TABLE `tbl_pinjam`
ADD CONSTRAINT `tbl_pinjam_ibfk_2` FOREIGN KEY (`id_jenis_peminjam`) REFERENCES `tbl_jenis_peminjam` (`id_jenis_peminjam`) ON DELETE CASCADE ON UPDATE CASCADE,
ADD CONSTRAINT `tbl_pinjam_ibfk_3` FOREIGN KEY (`id_barang`) REFERENCES `tbl_barang` (`id_barang`) ON DELETE CASCADE ON UPDATE CASCADE;

--
-- Constraints for table `tbl_user`
--
ALTER TABLE `tbl_user`
ADD CONSTRAINT `tbl_user_ibfk_1` FOREIGN KEY (`id_pegawai`) REFERENCES `tbl_pegawai` (`id_pegawai`) ON DELETE CASCADE ON UPDATE CASCADE;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
