<?php

 ?>
<!DOCTYPE html>
<html>
  <head>
    <title>Login</title>

      <!-- Bootstrap -->
      <link rel="stylesheet" href="assets/css/bootstrap.css"/>
      <link rel="stylesheet" href="assets/css/font-awesome.css"/>
      <!-- CSS -->
      <link rel="stylesheet" href="assets/css/styles.css"/>
      <link rel="stylesheet" href="assets/css/form-elements.css">
      <link href='assets/img/icon.png' rel='shortcut icon'>
      <!--Script Javascript-->
      <script type="text/javascript" src="assets/js/jquery-2.1.4.js"></script>
      <script src="assets/js/jquery.backstretch.min.js"></script>
      <script type="text/javascript" src="assets/js/bootstrap.js"></script>
      <style media="screen">
      .inner-bg {
      padding: 112px 0 170px 0;
      background-color: #3498db;
      }
      </style>
  </head>
  <body>

  <div class="top-content">
    <div class="inner-bg">
      <div class="container">
        <div class="row">
          <div class="col-sm-6 col-sm-offset-3 form-box">
            <div class="form-top">
              <div class="form-top-left">
                <h3>PEMINJAMAN BARANG LAB KOMPUTER</h3>
                  <p>Masukkan Username dan Password</p>
              </div>
              <div class="form-top-right">
                <i class="fa fa-sign-in"></i>
              </div>
              </div>
              <div class="form-bottom">
            <form action="" method="post" class="login-form">
              <div class="form-group">
                <label class="sr-only" for="form-username">Username</label>
                  <input type="text" name="username" placeholder="Username..." required="" class="form-username form-control" id="form-username">
                </div>
                <div class="form-group">
                  <label class="sr-only" for="form-password">Password</label>
                  <input type="password" name="password" placeholder="Password..." required="" class="form-password form-control" id="form-password">
                </div>
                <?php

                 ?>
                <button type="submit" class="btn" name="login">Login</button>
            </form>
          </div>
          </div>
        </div>
      </div>
    </div>
  </div>

  </body>
</html>
