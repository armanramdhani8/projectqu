<html>
    <style>
            *{
                font-size: 11px;
            }
            table{
                width:100%;
                max-width: 100%;
                border:1px solid #eee;
                border-collapse: collapse;
            }
            thead>tr>th, tbody>tr>td {
                border:1px solid #eee;
                padding: 5px;
                /* text-align: center; */
            }
            .text-center{
                text-align: center !important;
            }
    </style>
    <body>
        
        <table>
            <thead>
                <tr>
                    <th class="text-center">No.</th>
                    <th>Nama Peminjam</th>
                    <th>Jenis Peminjam</th>
                    <th>Jumlah Pinjam</th>
                    <th>Waktu Pinjam</th>
                    <th>Catatan</th>
                </tr>
            </thead>
            <tbody>
                <?php
                include("../koneksi.php");
                $no=0;
                $query = mysql_query("SELECT tbl_pinjam.*,table_jenis.nama_jenis,table_ruang.nama_ruang,table_petugas.nama_petugas FROM table_invent LEFT JOIN table_jenis ON table_invent.id_jenis=table_jenis.id_jenis LEFT JOIN table_ruang ON table_invent.id_ruang=table_ruang.id_ruang LEFT JOIN table_petugas ON table_invent.id_petugas=table_petugas.id_petugas ORDER BY kode_barang") or die (mysql_error());
                if (mysql_num_rows($query) == 0) {
                    echo '<tr><td colspan="11">Tidak ada Data!</td></tr>';
                }else{
                    while ($data = mysql_fetch_array($query)) {
                    $no++;
                ?>
                <tr>
                    <td><?php echo $no; ?></td>          
                    <td><?php echo $data['nama_peminjam']; ?></td>          
                    <td><?php echo $data['jenis_peminjam']; ?></td>
                    <td><?php echo $data['jumlah_pinjam']; ?></td>
                    <td><?php echo $data['waktu_pinjam']; ?></td>
                    <td><?php echo $data['catatan']; ?></td>
                </tr>
                <?php
                }
                }
                ?>
            </tbody>  
        </table>
    </body>
</html>