
   <!-- sidebar -->
 <div class="sidebar-wrapper shadow">
   <div class="sidebar-avatar">
     <div class="sidebar-avatar-icon">
       <img src="../assets/img/teacher_icon.png" class="img-circle" alt="Cinque Terre" width="150" height="150">
     </div>
     <div class="sidebar-avatar-name">
     </div>
     <div class="sidebar-avatar-level">
       Administrator
     </div>
   </div>
   <!-- sidebar menu -->
   <div class="sidebar-menu">
     <div class="panel-group" id="accordion1" role="tablist" aria-multiselectable="true">
       <div class="panel panel-default no-border no-radius" style="border-radius: 0px">
         <div class="panel-heading menu-parent" role="tab" id="headingOne" style="border-radius: 0px">
           <h4 class="panel-title" style="text-align: left;">
             <a href="beranda.php"><span class="fa fa-home" style="font-size: 20px;"></span>&nbsp;&nbsp; Beranda</a>

           </h4>
         </div>
         <div id="collapseOne" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" style="border-radius: 0px">

         </div>
       </div>

      <div class="panel panel-default no-border no-radius" style="border-radius: 0px">
       <div class="panel-heading menu-parent" role="tab" id="headingtwo" style="border-radius: 0px">
         <h4 class="panel-title" style="text-align: left;">
           <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseTwo" aria-expanded="true" aria-controls="collapseThree"><span class="fa fa-tag" style="font-size: 20px;"></span>&nbsp;&nbsp; Data Barang
           </a>
         </h4>
       </div>
       <div id="collapseTwo" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" style="border-radius: 0px">
         <div class="panel-body menu-child" style="border-radius: 0px">
           <ul>
             <a href="frmJenisBarang.php"><span class="fa fa-user" style="font-size: 20px;"></span>&nbsp;&nbsp; Jenis Barang</a><br>
             <a href="frmDataBarang.php"><span class="fa fa-user" style="font-size: 20px;"></span>&nbsp;&nbsp; Detail Barang</a>
           </ul>
         </div>
       </div>
      </div>

       <div class="panel panel-default no-border no-radius" style="border-radius: 0px">
         <div class="panel-heading menu-parent" role="tab" id="headingTwo" style="border-radius: 0px">
           <h4 class="panel-title">
             <a role="button" data-toggle="collapse" data-parent="#accordion2" href="#collapseThree" aria-expanded="true" aria-controls="collapseThree"><span class="fa fa-pencil" style="font-size: 20px;"></span>&nbsp;&nbsp; Peminjaman Barang
             </a>
           </h4>
         </div>
         <div id="collapseThree" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingFour" style="border-radius: 0px">
           <div class="panel-body menu-child" style="border-radius: 0px">
             <ul>
               <a href="frmStudent.php"><span class="fa fa-user" style="font-size: 20px;"></span>&nbsp;&nbsp; Siswa</a><br>
               <a href="frmTeacher.php"><span class="fa fa-user" style="font-size: 20px;"></span>&nbsp;&nbsp; Guru</a>
             </ul>
           </div>
         </div>
       </div>

       <div class="panel panel-default no-border no-radius" style="border-radius: 0px">
         <div class="panel-heading menu-parent" role="tab" id="headingOne" style="border-radius: 0px">
           <h4 class="panel-title" style="text-align: left;">
             <a href="frmDataPinjam.php"><span class="fa fa-book" style="font-size: 20px;"></span>&nbsp;&nbsp; Data Pinjam</a>

           </h4>
         </div>
         <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" style="border-radius: 0px">

         </div>
       </div>
        <div class="panel panel-default no-border no-radius" style="border-radius: 0px">
         <div class="panel-heading menu-parent" role="tab" id="headingOne" style="border-radius: 0px">
           <h4 class="panel-title" style="text-align: left;">
             <a href="frmDataPeminjaman.php"><span class="fa fa-folder" style="font-size: 20px;"></span>&nbsp;&nbsp; Data Peminjaman </a>

           </h4>
         </div>
         <div id="collapseFour" class="panel-collapse collapse" role="tabpanel" aria-labelledby="headingOne" style="border-radius: 0px">

         </div>
       </div>
         </div>
       </div>
     </div>
   </div>
 </div>
 <!-- Akhir Sidebar -->
