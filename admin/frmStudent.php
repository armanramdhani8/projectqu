<?php
include '../koneksi.php';
session_start();
if(!isset($_SESSION['login_user'])){
  header ('location:../admin/login.php');
}
?>
<!DOCTYPE html>
<html>
 <head>
   <title>INVENTARIS SKANIC</title>

      <!-- Bootstrap -->
      <link rel="stylesheet" href="../assets/css/bootstrap.css"/>
      <link rel="stylesheet" href="../assets/css/font-awesome.css"/>
      <link rel="stylesheet" href="../assets/css/custom.css"/>
      <link href='../assets/img/sknc.png' rel='shortcut icon'>
      <!-- Java Script -->
      <script type="text/javascript" src="../assets/js/jquery-2.1.4.js"></script>
      <script type="text/javascript" src="../assets/js/bootstrap.js"></script>

 </head>
 <body>
<?php
  include 'navbar.php';
  include 'sidebar.php';
?>
<div class="content" style="background-color:#ecf0f1;">
 <h2 style="margin-left:20px;"><span class="fa fa-tag" style="font-size: 30px;"></span>&nbsp;Pinjam Barang</h2>
 <div class="col-md-12">
   <ol class="breadcrumb" style="background-color:#FAFAFA;">
   <li><a href="#">Pinjam Barang</a> / <a href="#">Siswa</a></li>
   </ol>
     <!-- <div class="container"> -->
     <div class="panel-body">
        <div class="container-fluid" style="background: #FFF; padding: 10px; border-top: 3px solid #2980b9;">
          <form action="proses/s_simpanpinjam.php" method="post">
            <!-- <div class="form-group">
            <label for="idpeminjaman">ID Peminjaman :</label>
            <input type="text" class="form-control" id="id">
            </div> -->
            <!-- <div class="form-group">
            <label for="pwd">KTP :</label>
            <input type="text" class="form-control" id="nip">
            </div> -->
            <script type="text/javascript">
            function get_data_kelas(value){
              $.ajax({
                url: '../admin/proses/a_getdatasiswa.php',
                type: 'POST',
                data: {
                  nama_siswa: value
                }
              })
              .done(function(data) {
                var _data = JSON.parse(data);
                if(_data.result == 1){
                  $(document).find('.isinya').html(_data.html);
                }else{
                  $(document).find('.isinya').html('<p>Tidak ada data.</p>');
                }
                console.log("success");
              });
            }
            $(document).on('click','.pilih_siswanya',function(evt){
              var id_siswa = $(this).attr('id_siswa');
              var kelas = $(this).attr('kelas');
              var nama_siswa = $(this).attr('nama_siswa');
              $(document).find('#id_siswa').val(id_siswa);
              $(document).find('#nama_siswa').val(nama_siswa);
              $(document).find('#kelas_siswa').val(kelas);
            });
            </script>
            <div class="modal fade" id="modal_cari_siswa">
              <div class="modal-dialog">
                <div class="modal-content">
                  <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-label="Close"><span aria-hidden="true">&times;</span></button>
                    <h4 class="modal-title">Pilih Siswa</h4>
                  </div>
                  <div class="modal-body">
                    <label for=""><span class="fa fa-search"></span>&nbsp;Cari Siswa</label>
                    <input type="text" name="" class="form-control" value="" onkeyup="get_data_kelas(this.value)">
                    <div class="isinya">

                    </div>
                  </div>
                  <div class="modal-footer">
                    <button type="button" class="btn btn-default" data-dismiss="modal">Tutup</button>
                  </div>
                </div><!-- /.modal-content -->
              </div><!-- /.modal-dialog -->
            </div><!-- /.modal -->
            <script type="text/javascript">
              $(document).on('click','#nama_siswa',function(evt){
                evt.preventDefault();
                $('#modal_cari_siswa').modal('show');
                // data-toggle="modal" data-target="#myModal"
              });
            </script>
            <div class="form-group">
            <label for="pwd">Nama Siswa :</label> <br>
            <small>Note : Jika peminjaman perkelas, ketua/wakil ketua/sekretatis yang mewakilkan</small>
            <input type="text" class="form-control" name="nama_siswa" required="" id="nama_siswa" readonly="">
            <input type="hidden" class="form-control" name="id_siswa" required="" id="id_siswa" readonly="">
            </div>
            <div class="form-group">
            <label for="pwd">Kelas :</label>
            <input type="text" class="form-control" name="kelas_siswa" required="" id="kelas_siswa" readonly="">
            </div>
            <script type="text/javascript">
            function get_nama_barang(value){
              $.ajax({
                url: 'proses/a_get_data_barang.php',
                type: 'POST',
                data: {
                  id_jenis_barang: value
                }
              })
              .done(function(data) {
                var _data = JSON.parse(data);
                if(_data.result == 1){
                  $(document).find('.id_barang').html(_data.html);
                }else{
                  $(document).find('.id_barang').html("");
                  alert('Barang yang anda maksud stok nya kosong.');
                }
                console.log("success");
              });
            }
            </script>
            <div class="form-group">
            <label for="pwd">Jenis Barang :</label>
            <select class="form-control" name="jenis_barang" required="" onchange="get_nama_barang(this.value)">
              <option value="">Pilih</option>
              <?php
              $query = "SELECT * FROM tbl_jenis_barang";
              $select = mysql_query($query);
              while ($data = mysql_fetch_assoc($select)) {?>
                <option value="<?=$data['id_jenis_barang']?>"><?=$data['nama_jenis']?></option>
              <?php
              }
              ?>
            </select>
            </div>
            <script type="text/javascript">
            function show_nama_barang(value){
              if(parseInt(value) < 2){
                $(document).find('#single').show(500);
                $(document).find('#multiple').hide(500);
              }else if(parseInt(value) > 1){
                $(document).find('#single').hide(500);
                $(document).find('#multiple').show(500);
              }
            }
            </script>
            <!-- <div class="form-group">
            <label for="idpeminjaman">Jumlah Pinjam :</label>
            <input type="number" class="form-control" name="jumlah_pinjam" min="0" required="" onkeyup="show_nama_barang(this.value)">
            </div> -->
            <!-- <div class="form-group" style="display: none" id="single">
            <label for="pwd">Nama Barang :</label>
            <select class="form-control id_barang" name="id_barang[]" required="">
              <option value="" disabled="">Pilih</option>
            </select> -->
            <!-- </div> -->
            <div class="form-group" style="display: block" id="">
            <label for="pwd">Nama Barang :</label>
            <select multiple class="form-control id_barang" name="id_barang[]" required="">
              <option value="" disabled="">Pilih</option>
            </select>
            </div>
            <?php
              $oke = date('d-m-Y');
          ?>
         
            <div class="form-group">
            <label for="pwd">Tanggal : </label>
            <input type="text" class="form-control" name="waktu_pinjam" value="<?php echo 
            $oke; ?>" readonly>
            </div>
            <div class="form-group">
            <label for="pwd">Catatan Pinjam</label>
            <input type="text" class="form-control" name="catatan">
            </div>
            <button type="submit" class="btn btn-primary">Simpan</button>
          </form>
     </div>
   </div>
         <!-- Penutup warna warni -->

     <!-- </div> -->
   </div>
 </div>
 </body>
</html>
