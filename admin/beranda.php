<?php
include '../koneksi.php';
session_start();
if(!isset($_SESSION['login_user'])){
  header ('location:../admin/login.php');
}
?>

<!DOCTYPE html>
<html>
 <head>
   <title>INVENTARIS SKANIC</title>

      <!-- Bootstrap -->
      <link rel="stylesheet" href="../assets/css/bootstrap.css"/>
      <link rel="stylesheet" href="../assets/css/font-awesome.css"/>
      <link rel="stylesheet" href="../assets/css/custom.css"/>
      <link href='../assets/img/sknc.png' rel='shortcut icon'>
      <!-- Java Script -->
      <script type="text/javascript" src="../assets/js/jquery-2.1.4.js"></script>
      <script type="text/javascript" src="../assets/js/bootstrap.js"></script>

 </head>
 <body>
 <?php
  include "sidebar.php";
  include "navbar.php";
 ?>

  <div class="content" style="background-color:#ecf0f1;">
   <h2 style="margin-left:20px;"><span class="fa fa-home" style="font-size: 30px;"></span>&nbsp;Beranda</h2>
   <sdiv class="col-md-12">
     <ol class="breadcrumb" style="background-color:#FAFAFA;">
     <li><a href="#">Beranda</a></li>
     </ol>

    <div class="container-fluid" style="background: #FFF; padding: 10px; border-top: 3px solid #2980b9;">
    <!--<div class="container" style="margin: 10px 50px">-->
      <?php
        $query = "SELECT * FROM tbl_barang AS b, tbl_jenis_barang AS jb WHERE b.id_jenis_barang=jb.id_jenis_barang GROUP BY b.id_jenis_barang";
        $select = mysql_query($query);
        while ($data = mysql_fetch_assoc($select)) {
          $qJumlahBarang = "SELECT COUNT('id_barang') AS total FROM tbl_barang WHERE id_jenis_barang = ".$data['id_jenis_barang'];
          $sJumlahBarang = mysql_query($qJumlahBarang);
          $dJumlahBarang = mysql_fetch_assoc($sJumlahBarang);
          $cJumlahBarang = $dJumlahBarang['total'];
          $qSisaBarang = "SELECT COUNT('id_barang') AS total FROM tbl_barang WHERE id_jenis_barang = ".$data['id_jenis_barang']." AND status_barang = 0";
          $sSisaBarang = mysql_query($qSisaBarang);
          $dSisaBarang = mysql_fetch_assoc($sSisaBarang);
          $cSisaBarang = $dSisaBarang['total'];
          ?>
        <!-- Warna Warni -->
          <div class="barang-wrapper col-lg-3 col-md-3 col-sm-6 col-xs-12" style="height: 130px; padding: 10px;">
            <div class="barang-content" style="background: red; width: 100%; height: 100%">
              <div class="barang-left-side" style="height: 100%; width: 50%; background: #f39c12; color: #FFF; float: left">
                <img src="../assets/img/barang/<?=$data['gambar'];?>" alt="" style="height: 100%; width: 100%">
              </div>
              <div class="barang-right-side" style="height: 100%; width: 50%; float: right">
                <div class="barang-count" style="width: 100%; height: 70%; background: #d35400; color: #FFF; font-size: 45px;">
                  <table width="100%" height="100%">
                    <th valign="middle">
                      <td align="center"><?=$cSisaBarang."/".$cJumlahBarang;?><div style="font-size: 15px; margin-top: -15px">Tersisa</div></td>
                    </th>
                  </table>
                </div>
                <div class="barang-name" style="width: 100%; height: 30%; background: #e67e22; color: #FFF; font-size: 17px;">
                  <table width="100%" height="100%">
                    <th valign="middle">
                      <td align="center"><?=$data['nama_jenis'];?></td>
                    </th>
                  </table>
                </div>
              </div>
            </div>
          </div>
          <!-- Penutup warna warni -->
        <?php
        }
        ?>
        </div>
    </div>
  </div>
  </div>
</div>
</div>
    </div>
  </div>
 </body>
</html>
