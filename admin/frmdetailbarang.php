<?php
include '../koneksi.php';
session_start();
if(!isset($_SESSION['login_user'])){
  header ('location:../admin/login.php');
}
?>

 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <title>Tambah Data</title>
     <link rel="stylesheet" href="../assets/css/bootstrap.css"/>
     <link rel="stylesheet" href="../assets/css/font-awesome.css"/>
     <link rel="stylesheet" href="../assets/css/custom.css"/>
     <link href='../assets/img/icon.png' rel='shortcut icon'>
     <!-- Java Script -->
     <script type="text/javascript" src="../assets/js/jquery-2.1.4.js"></script>
     <script type="text/javascript" src="../assets/js/bootstrap.js"></script>
   </head>
   <body>
  <?php
    include 'navbar.php';
    include 'sidebar.php';
  ?>

  <div class="content" style="background-color:#ecf0f1;">
   <h2 style="margin-left:20px;"><span class="fa fa-tag" style="font-size: 30px;"></span>&nbsp;Detail Barang</h2>
   <div class="col-md-12">
     <ol class="breadcrumb" style="background-color:#FAFAFA;">
     <li><a href="#">Detail Barang</a>
     </ol>
       <!-- <div class="container"> -->
       <div class="panel-body">
            <a href="" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span> &nbsp;&nbsp;Tambah</a>
           <br>
           <br>
           <div class="container-fluid" style="background: #FFF; padding: 10px; border-top: 3px solid #2980b9;">
           <table class="table table-hover">
             <thead>
               <tr>
                 <th>No.</th>
                 <th>Id Barang</th>
                 <th>Nama Barang</th>
                 <th>Merk</th>
                 <th>Kondisi</th>
                 <th>Tanggal</th>
                 <th>Level Barang</th>
                 <th>Keterangan</th>
                 <th>Aksi</th>
               </tr>
             </thead>
             <?php
          		$query = "SELECT * FROM tbl_jenis_barang AS jb, tbl_detail_barang AS db, tbl_barang AS b WHERE db.`id_ barang`= b.`id_barang` AND jb.`id_jenis_barang`=b.`id_jenis_barang`";
          		$result = mysql_query($query);
          		$no = 1;
          		while ($data = mysql_fetch_assoc($result)){
          		?>
             <tbody>
               <tr>
                 <td><?=$no;?></td>
                 <td><?=$data['id_barang'];?></td>
                 <td><?=$data['nama_jenis'];?></td>
                 <td><?=$data['merk'];?></td>
                 <td><?=$data['kondisi'];?></td>
                 <td><?=$data['tanggal'];?></td>
                 <td><?=$data['level_barang'];?></td>
                 <td><?=$data['keterangan'];?></td>
                 <td>
                   <div class="btn-group">
                     <button type="button" class="btn btn-warning" data-toggle="modal" data-target="#myModal2"><span class="fa fa-edit"></span>&nbsp;&nbsp;Ubah</button>
                     <button type="button" class="btn btn-danger"><span class="fa fa-trash"></span>&nbsp;&nbsp;Hapus</button>
                   </div>
                 </td>
               </tr>
            		<?php
            		$no = $no +1;
            		}
            		?>
             </tbody>
           </table>
         </div>
       </div>
       </div>
   </div>


  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Detail Barang</h4>
      </div>
      <div class="modal-body">
          <form class="" action="#" method="post">
            <?php
  					$idbarang = mysql_fetch_assoc(mysql_query("SELECT COUNT(id_detail_barang) AS total FROM tbl_detail_barang"));
  					$query = "select * from tbl_detail_barang";
  					$no = $idbarang['total'];
  					$no = $no + 1;
  					$idbarangFix = "";
  					if($idbarang['total'] < 1){
  						$idbarangFix = "DB001";
  					}else if($idbarang['total'] < 10){
  						$idbarangFix = "DB00".$no;
  					}else if($idbarang['total'] < 100){
  						$idbarangFix = "DB0".$no;
  					}
  					$result = mysql_query($query);
  					while ($tampil = mysql_fetch_array($result)){
  						?>

  					<?php
  						}
  					?>
            <div class="form-group">
            <label for="pwd">Id Detail Barang :</label>
            <input type="text" class="form-control" readonly="" name="id_barang" value="<?=$idbarangFix;?>">
            </div>
            <div class="form-group">
            <label for="pwd">Id Barang :</label>
            <input type="text" class="form-control" name="id_barang">
            </div>
            <div class="form-group">
            <div class="col-sm-pull-3" align="left">
              <label for="pwd">Jenis Barang :</label><br>
              <select name="slct1" class="btn btn-default" id="slct1" onchange="populate(this.id,'slct2')">
                <?php
                    $test = "SELECT * FROM tbl_jenis_barang";
                    $q = mysql_query($test);
                     while ($data = mysql_fetch_array($q))
                     {

                ?>
                <option value="<?php echo $data ['id_jenis_barang'] ?>">
                    <?php echo $data ['nama_jenis'] ?>
              <?php } ?>
              </option>
              </select>
            </div>
          </div>
            <div class="form-group">
            <label for="pwd">Nama Barang :</label>
            <input type="text" class="form-control" name="nama_barang">
            </div>
            <div class="form-group">
            <label for="pwd">Merk :</label>
            <input type="text" class="form-control" name="merk">
            </div>
            <div class="form-group">
            <label for="pwd">Kondisi :</label>
            <input type="text" class="form-control" name="kondisi">
            </div>
            <div class="form-group">
            <label for="pwd">Tanggal :</label>
            <input type="date" class="form-control" name="tanggal">
            </div>
            <div class="form-group">
            <label for="pwd">Level Barang :</label>
            <input type="text" class="form-control" name="level_barang">
            </div>
            <div class="form-group">
            <label for="pwd">Keterangan :</label>
            <input type="text" class="form-control" name="keterangan">
            </div>
          </form>
      </div>
      <div class="modal-footer">
      <a href="#" class="btn btn-primary" name="simpan">Simpan</a>
      </div>
    </div>

  </div>
  </div>
  <!-- End Modal -->

  <!-- Modal Edit -->
  <div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
          <form class="" action="#" method="post">
            <div class="form-group">
            <label for="pwd">Id Barang :</label>
            <input type="text" class="form-control" name="id_barang">
            </div>
            <div class="form-group">
            <label for="pwd">Nama Barang :</label>
            <input type="text" class="form-control" name="nama_barang">
            </div>
            <div class="form-group">
            <label for="pwd">Merk :</label>
            <input type="text" class="form-control" name="merk">
            </div>
            <div class="form-group">
            <label for="pwd">Kondisi :</label>
            <input type="text" class="form-control" name="kondisi">
            </div>
            <div class="form-group">
            <label for="pwd">Tanggal :</label>
            <input type="date" class="form-control" name="tanggal">
            </div>
            <div class="form-group">
            <label for="pwd">Level Barang :</label>
            <input type="text" class="form-control" name="level_barang">
            </div>
            <div class="form-group">
            <label for="pwd">Keterangan :</label>
            <input type="text" class="form-control" name="keterangan">
            </div>
          </form>
      </div>
      <div class="modal-footer">
      <a href="#" class="btn btn-primary" name="simpan">Simpan</a>
      </div>
    </div>

  </div>
  </div>
  <!-- End Modal Edit -->


   </body>
 </html>
