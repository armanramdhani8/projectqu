<!-- Static navbar -->
 <nav class="navbar navbar-default navbar-fixed-top no-border no-radius shadow">
   <div class="container">
     <div class="navbar-header">
       <button type="button" class="navbar-toggle collapsed" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
         <span class="sr-only">Toggle navigation</span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
         <span class="icon-bar"></span>
       </button>
       <a class="navbar-brand" href="#" style="margin-top: -3px;">INVENTARISIR <span class="glyphicon glyphicon-edit logo-small" style="font-size: 25px;"></span></a>
       </div>
     <script type="text/javascript">
      function tanggal(){
        var months = ['Januari', 'Februari', 'Maret', 'April', 'Mei', 'Juni', 'Juli', 'Agustus', 'September', 'Oktober', 'November', 'Desember'];
        var myDays = ['Minggu', 'Senin', 'Selasa', 'Rabu', 'Kamis', 'Jum&#39;at', 'Sabtu'];
        var date = new Date();
        var day = date.getDate();
        var month = date.getMonth();
        var thisDay = date.getDay(),
            thisDay = myDays[thisDay];
        var yy = date.getYear();
        var year = (yy < 1000) ? yy + 1900 : yy;
        return thisDay + ', ' + day + ' ' + months[month] + ' ' + year;
      }
      function time(){
        var d = new Date();
         var curr_hour = d.getHours();
         var curr_min = d.getMinutes();
         var curr_sec = d.getSeconds();
         curr_min = curr_min + "";
         if (curr_min.length == 1) {
             curr_min = "0" + curr_min;
         }
         return curr_hour + " : " + curr_min + " : " + curr_sec;
      }
      $(document).ready(function() {
        $('#clock').html("  "+tanggal()+" "+time());
         setInterval(function () {
           $('#clock').html("  "+tanggal()+" "+time());
         }, 1000);
      });
     </script>
         <div id="navbar" class="navbar-collapse collapse">
             <ul class="nav navbar-nav navbar-right">
               <li class=""><a href="./"><span class="fa fa-calendar" id="clock"></span></a></li>

            <ul class="nav navbar-nav navbar-right">
              <li><a href="#" data-toggle="modal" data-target="#myModal3">Logout</a></li>
            </ul>
         </div>
       </ul>
     </div><!--/.nav-collapse -->
   </div><!--/.container-fluid -->
 </nav>

 <!-- Modal -->
<div id="myModal3" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Logout</h4>
      </div>
      <div class="modal-body">
        <p>Yakin ingin keluar?</p>
      </div>
      <div class="modal-footer">
        <a href="../logout.php" class="btn btn-primary">Keluar</a>
      </div>
    </div>
  </div>
</div>
<!-- Penutup Modal -->

 <script type="text/javascript">
   $(document).ready(function() {
     var _window = $(window);
     $(window).resize(function(){
       if($(window).outerWidth() < 768){
         $('.sidebar-wrapper').css('left','-300px');
         $('body').css('margin-left','10px');
       }else{
         $('.sidebar-wrapper').css('left','0px');
         $('body').css('margin-left','260px');
       }
     });
   });
 </script>
