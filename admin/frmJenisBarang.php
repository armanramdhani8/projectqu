<?php
include '../koneksi.php';
session_start();
if(!isset($_SESSION['login_user'])){
  header ('location:../admin/login.php');
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Tampil Data</title>
    <link rel="stylesheet" href="css/style.css">

     <!-- PANGGIL CSS NYA BOOSTRAP -->
     <link href="css/bootstrap.min.css" rel="stylesheet">

     <!-- PANGGIL CSS NYA Data Tables -->
     <link rel="stylesheet" href="../css/dataTables.bootstrap.min.css">
     <link rel="stylesheet" href="../assets/css/bootstrap.css"/>
     <link rel="stylesheet" href="../assets/css/font-awesome.css"/>
     <link rel="stylesheet" href="../assets/css/custom.css"/>
     <link href='../assets/img/sknc.png' rel='shortcut icon'>
     <link rel="stylesheet" href="../dataTables/css/dataTables.bootstrap.min.css"/>
     <script type="text/javascript" src="../assets/js/jquery-2.1.4.js"></script>
     <script type="text/javascript" src="../assets/js/bootstrap.js"></script>

    <script type="text/javascript" language="JavaScript">
     function konfirmasidelete()
     {
     tanya = confirm("Anda Yakin Akan Menghapus Data ?");
     if (tanya == true) return true;
     else return false;
     }
     </script>

</head>
 <body>
  <?php
    include 'navbar.php';
    include 'sidebar.php';
  ?>

 <div class="content" style="background-color:#ecf0f1;">
   <h2 style="margin-left:20px;"><span class="fa fa-tag" style="font-size: 30px;"></span>&nbsp;Data Barang</h2>
   <div class="col-md-12">
     <ol class="breadcrumb" style="background-color:#FAFAFA;">
     <li><a href="frmDatabarang.php">Data Barang</a> / <a href="#">Jenis Barang</a></li>
     </ol>
       <!-- <div class="container"> -->
       <div class="panel-body">
          <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span> &nbsp;&nbsp;Tambah</a>
          <a href="lap_pdf.php" class="btn btn-danger">PDF</a></p>
           <br>
           <br>
           <div class="container-fluid" style="background: #FFF; padding: 10px; border-top: 3px solid #2980b9;">
           <table class="table table-hover" id="zz">
             <thead>
               <tr>
                 <th>No.</th>
                 <th>Nama Jenis Barang</th>
                 <th>Opsi</th>
               </tr>
             </thead>
             <tbody>
             <?php
               $query = "SELECT * FROM tbl_jenis_barang";
               $result = mysql_query($query);
               $no = 1;
               while ($tampil = mysql_fetch_array($result)){
              ?>
               <tr>
                <td><?=$no;?></td>
                <td id="id_barang<?=$no;?>"><?php echo $tampil['nama_jenis']; ?></td>
                 <td>
                   <div class="btn-group">
                     <button class="tbl_ubah btn btn-primary" type="button" id_jenis_barang="<?=$tampil['id_jenis_barang']?>" nama_jenis="<?=$tampil['nama_jenis']?>" data-toggle="modal" data-target="#myModal2"><span class="fa fa-edit"></span>&nbsp;&nbsp;Ubah</button>
                     <a href="proses/p_hapusjenisbarang.php?id_jenis_barang=<?=$tampil['id_jenis_barang']?>" class="btn btn-danger"><span class="fa fa-trash"></span>&nbsp;&nbsp;Hapus</a>
                   </div>
                 </td>
                 <?php
                  $no = $no +1;
                  }
                  ?>
               </tr>
             </tbody>
           </table>
         </div>
       </div>
   </div>
   <script type="text/javascript">
     $(document).on('click','.tbl_ubah',function(evt){
      evt.preventDefault();
      var id_jenis_barang = $(this).attr('id_jenis_barang');
      var nama_jenis      = $(this).attr('nama_jenis');
      $(document).find('.id_jenis_barang').val(id_jenis_barang);
      $(document).find('.nama_jenis').val(nama_jenis);
     });
   </script>

  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Barang</h4>
      </div>
      <div class="modal-body">
        <form class="" action="proses/p_tambahjenisbarang.php" method="post" enctype="multipart/form-data">
          <div class="form-group">
          <label for="pwd">Nama Jenis Barang :</label>
          <input type="text" class="form-control" name="nama_jenis">
          </div>
      </div>
      <div class="modal-footer">
        <input type="submit" value="Kirim" name="kirim" class="btn btn-primary">
        </form>
      </div>
    </div>

  </div>
  </div>
  <!-- End Modal -->

  <!-- Modal Edit -->
  <div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content -->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ubah Jenis Barang</h4>
      </div>
      <div class="modal-body">
        <form class="" action="proses/p_ubahjenisbarang.php" method="post" enctype="multipart/form-data">

          <div class="form-group">
          <label for="pwd">Id Jenis Barang :</label>
          <input type="text" class="form-control id_jenis_barang" name="id_jenis_barang" value="" readonly="">
          </div>
          <div class="form-group">
          <label for="pwd">Nama Jenis :</label>
          <input type="text" class="form-control nama_jenis" name="nama_jenis" value="">
          </div>
      </div>
      <div class="modal-footer">
      <input type="submit" class="btn btn-primary" name="simpan" value="Ubah">
      </form>
      </div>
    </div>

  </div>
  </div>
  <!-- End Modal Edit -->
    <!-- PANGGIL JQUERY nya -->
     <script type="text/javascript" src="../dataTables/js/jquery.js"></script>

     <!-- PANGGIL JQUERY nya Bootstrap -->
     <script type="text/javascript" src="../dataTables/js/bootstrap.min.js"></script>

     <!-- PANGGIL js JQUERY datatables nya -->
     <script type="text/javascript" src="../dataTables/js/jquery.dataTables.min.js"></script>

     <!-- PANGGIL js nya Datatables Bootstrap -->
     <script type="text/javascript" src="../dataTables/js/dataTables.bootstrap.min.js"></script>
    

     <script type="text/javascript">
        $(function(){
            $("#zz").dataTable();
        }); 
     </script>

  <!-- check all checkbox -->
  <script language="javascript">
    function toggle(asd) {
        checkboxes = document.getElementsByName('foo');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = asd.checked;
        }
    }
</script>
</body>
</html>
