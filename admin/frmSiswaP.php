<?php
session_start();
if(!isset($_SESSION['login_user'])){
  header ('location:../admin/login.php');
}
?>
<!DOCTYPE html>
<html>
 <head>
   <title>Peminjaman Barang Laboratorium Komputer SMK Negeri 1 Ciomas</title>

      <!-- Bootstrap -->
      <link rel="stylesheet" href="../assets/css/bootstrap.css"/>
      <link rel="stylesheet" href="../assets/css/font-awesome.css"/>
      <link rel="stylesheet" href="../assets/css/custom.css"/>
      <link href='../assets/img/icon.png' rel='shortcut icon'>
      <!-- Java Script -->
      <script type="text/javascript" src="../assets/js/jquery-2.1.4.js"></script>
      <script type="text/javascript" src="../assets/js/bootstrap.js"></script>

 </head>
 <body>
   <?php include 'navbar.php'; ?>
   <?php include 'sidebar.php'; ?>

               <div class="content" style="background-color:#ecf0f1;">
                <h2 style="margin-left:20px;"><span class="fa fa-tag" style="font-size: 30px;"></span>&nbsp;Pinjam Barang</h2>
                <div class="col-md-12">
                  <ol class="breadcrumb" style="background-color:#FAFAFA;">
                  <li><a href="#">Pinjam Barang</a> / <a href="#">Perorangan</a></li>
                  </ol>
                  <div class="panel-body">
                    <a href="frmsiswa.php" class="btn btn-primary"> Kelas</a>
                    <a href="#" class="btn btn-primary"> Perorangan</a>
                    <br>
                    <br>

                    <!-- <div class="container"> -->
                    <div class="container-fluid" style="background: #FFF; padding: 10px; border-top: 3px solid #2980b9;">
                    <form class="" action="index.html" method="post">
                      <div class="form-group">
                      <label for="idpeminjaman">ID Peminjaman :</label>
                      <input type="text" class="form-control" id="id">
                      </div>
                      <div class="form-group">
                      <label for="pwd">NIS :</label>
                      <input type="text" class="form-control" id="nip">
                      </div>
                      <div class="form-group">
                      <label for="pwd">Nama :</label>
                      <input type="text" class="form-control" id="nama">
                      </div>

                      <div class="form-group">
                        <div class="col-sm-pull-3" align="left">
                          <label for="pwd"> Barang :</label><br>
                          <select name="slct1" class="btn btn-default" id="slct1">
                            <option value="laptop">Laptop</option>
                            <option value="headset">Headset</option>
                            <option value="kabel">Kabel</option>
                            <option value="cd">CD</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                        <div class="col-sm-pull-3" align="left">
                          <label for="pwd"> Nama Barang :</label><br>
                          <select name="slct2" class="btn btn-default" id="slct2">
                            <option value="acer">Acer</option>
                            <option value="hp">HP</option>
                            <option value="lenovo">Lenovo</option>
                            <option value="cduji">CD Uji kom</option>
                          </select>
                        </div>
                      </div>

                      <div class="form-group">
                      <label for="pwd">Nama Barang :</label>
                      <input type="text" class="form-control" id="nama">
                      </div>
                      <div class="form-group">
                      <label for="pwd">Waktu Pinjam :</label>
                      <input type="date" class="form-control" id="waktu">
                      </div>
                      <a href="#" class="btn btn-primary"> Simpan</a>
                    </form>
                  </div>
                </div>
                        <!-- Penutup warna warni -->

                    <!-- </div> -->
                  </div>
                </div>
               </div>
 </body>
</html>
