<?php
include '../koneksi.php';
session_start();
if(!isset($_SESSION['login_user'])){
  header ('location:../admin/login.php');
}
?>

<!DOCTYPE html>
<html>
<head>
    <title>Tampil Data</title>
    <link rel="stylesheet" href="css/style.css">

     <!-- PANGGIL CSS NYA BOOSTRAP -->
     <link href="css/bootstrap.min.css" rel="stylesheet">

     <!-- PANGGIL CSS NYA Data Tables -->
     <link rel="stylesheet" href="../css/dataTables.bootstrap.min.css">
     <link rel="stylesheet" href="../assets/css/bootstrap.css"/>
     <link rel="stylesheet" href="../assets/css/font-awesome.css"/>
     <link rel="stylesheet" href="../assets/css/custom.css"/>
     <link href='../assets/img/sknc.png' rel='shortcut icon'>
     <link rel="stylesheet" href="../dataTables/css/dataTables.bootstrap.min.css"/>
     <script type="text/javascript" src="../assets/js/jquery-2.1.4.js"></script>
     <script type="text/javascript" src="../assets/js/bootstrap.js"></script>

    <script type="text/javascript" language="JavaScript">
     function konfirmasidelete()
     {
     tanya = confirm("Anda Yakin Akan Menghapus Data ?");
     if (tanya == true) return true;
     else return false;
     }
     </script>

</head>
 <body>
  <?php
    include 'navbar.php';
    include 'sidebar.php';
  ?>

<div class="content" style="background-color:#ecf0f1;">
   <h2 style="margin-left:20px;"><span class="fa fa-tag" style="font-size: 30px;"></span>&nbsp;Data Peminjaman</h2>
   <div class="col-md-12">
     <ol class="breadcrumb" style="background-color:#FAFAFA;">
     <li><a href="frmDataPeminjaman.php">Data Peminjaman</a></li>
     </ol>
       <!-- <div class="container"> -->
       <div class="panel-body">
           <div class="container-fluid" style="background: #FFF; padding: 10px; border-top: 3px solid #2980b9;">
           <table class="table table-hover" id="aa">
             <thead>
               <tr>
                <th>No.</th>
                 <th>Nama Peminjam</th>
                 <th>Jenis Peminjam</th>
                 <th>Nama Barang</th>
                 <th>Waktu Pinjam</th>
                 <th>Status</th>
               </tr>
             </thead>
            <tbody>
             <?php
               $query = "SELECT * FROM
                        tbl_pinjam AS p,
                        tbl_barang AS b
                        WHERE p.`id_barang`=b.`id_barang`";
               $result = mysql_query($query);
               $no = 1;
               while ($tampil = mysql_fetch_array($result)){
                 //  siswa
                $status = "";
               if ($tampil['status_pinjam']==1){
                  $status="Sudah Dikembalikan";
               }else{
                  $status="Sedang Meminjam";
               }
                 if($tampil['id_jenis_peminjam'] == 1){
                   $query2 = "SELECT * FROM
                              tbl_siswa AS s,
                              tbl_pinjam AS pm,
                              tbl_barang AS b
                              WHERE
                              s.`id_siswa`=pm.`id_peminjam` AND
                              b.`id_barang`=pm.`id_barang` AND
                              pm.`id_peminjam` = ".$tampil['id_peminjam'];
                   $select2 = mysql_query($query2);
                   if ($data2 = mysql_fetch_assoc($select2)) { ?>
                     <tr>
                       <td><?=$no;?></td>
                       <td><?=$data2['nama'];?></td>
                       <td>Siswa|<?=$data2['kelas'];?></td>
                       <td><?=$tampil['nama_barang']; ?></td>
                       <td><?=$tampil['waktu_pinjam'];?></td>
                       <td><?php echo $status; ?></td>
                     </tr>
                   <?php
                   }
                 }
                //  guru
                 else if($tampil['id_jenis_peminjam'] == 2){
                   $query2 = "SELECT * FROM
                              tbl_pegawai AS pg,
                              tbl_pinjam AS pm,
                              tbl_barang AS b
                              WHERE
                              pg.`id_pegawai`=pm.`id_peminjam` AND
                              b.`id_barang`=pm.`id_barang` AND
                              pm.`id_peminjam` = ".$tampil['id_peminjam'];
;
                   $select2 = mysql_query($query2);
                   if ($data2 = mysql_fetch_assoc($select2)) { ?>
                     <tr>
                       <td><?=$no;?></td>
                       <td><?=$data2['nama'];?></td>
                       <td>Pegawai|<?=$data2['jabatan'];?></td>
                       <td><?=$tampil['nama_barang']; ?></td>
                       <td><?=$tampil['waktu_pinjam'];?></td>
                       <td><?php echo $status; ?></td>
                     </tr>
                   <?php
                   }
                 }
              ?>
               <?php
                $no++;
                }
                ?>
             </tbody>
           </table>
         </div>
       </div>
   </div>
   <script type="text/javascript">
     $(document).on('click','.tbl_ubah',function(evt){
      evt.preventDefault();
      var id_jenis_barang = $(this).attr('id_jenis_barang');
      var nama_jenis      = $(this).attr('nama_jenis');
      $(document).find('.id_jenis_barang').val(id_jenis_barang);
      $(document).find('.nama_jenis').val(nama_jenis);
     });
   </script>

  <!-- Modal Edit -->
  <div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content -->
    <<div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Ubah Jenis Barang</h4>
      </div>
      <div class="modal-body">
        <form class="" action="proses/p_ubahjenisbarang.php" method="post" enctype="multipart/form-data">

          <div class="form-group">
          <label for="pwd">Id Jenis Barang :</label>
          <input type="text" class="form-control id_jenis_barang" name="id_jenis_barang" value="" readonly="">
          </div>
          <div class="form-group">
          <label for="pwd">Nama Jenis :</label>
          <input type="text" class="form-control nama_jenis" name="nama_jenis" value="">
          </div>
      </div>
      <div class="modal-footer">
      <input type="submit" class="btn btn-primary" name="simpan" value="Ubah">
      </form>
      </div>
    </div>

  </div>
  <!-- End Modal Edit -->
    <!-- PANGGIL JQUERY nya -->
     <script type="text/javascript" src="../dataTables/js/jquery.js"></script>

     <!-- PANGGIL JQUERY nya Bootstrap -->
     <script type="text/javascript" src="../dataTables/js/bootstrap.min.js"></script>

     <!-- PANGGIL js JQUERY datatables nya -->
     <script type="text/javascript" src="../dataTables/js/jquery.dataTables.min.js"></script>

     <!-- PANGGIL js nya Datatables Bootstrap -->
     <script type="text/javascript" src="../dataTables/js/dataTables.bootstrap.min.js"></script>
    

     <script type="text/javascript">
        $(function(){
            $("#aa").dataTable();
        }); 
     </script>

  <!-- check all checkbox -->
  <script language="javascript">
    function toggle(asd) {
        checkboxes = document.getElementsByName('foo');
        for(var i=0, n=checkboxes.length;i<n;i++) {
            checkboxes[i].checked = asd.checked;
        }
    }
</script>
</body>
</html>
