<?php
include '../koneksi.php';
session_start();
if(!isset($_SESSION['login_user'])){
  header ('location:../admin/login.php');
}
?>
<!DOCTYPE html>
<html>
 <head>
   <title>Peminjaman Barang Laboratorium Komputer SMK Negeri 1 Ciomas</title>

      <!-- Bootstrap -->
      <link rel="stylesheet" href="../assets/css/bootstrap.css"/>
      <link rel="stylesheet" href="../assets/css/font-awesome.css"/>
      <link rel="stylesheet" href="../assets/css/custom.css"/>
      <link href='../assets/img/icon.png' rel='shortcut icon'>
      <!-- Java Script -->
      <script type="text/javascript" src="../assets/js/jquery-2.1.4.js"></script>
      <script type="text/javascript" src="../assets/js/bootstrap.js"></script>

 </head>
 <body>
<?php
  include 'navbar.php';
  include 'sidebar.php';
?>

<div class="content" style="background-color:#ecf0f1;">
 <h2 style="margin-left:20px;"><span class="fa fa-tag" style="font-size: 30px;"></span>&nbsp;Pinjam Barang</h2>
 <div class="col-md-12">
   <ol class="breadcrumb" style="background-color:#FAFAFA;">
   <li><a href="#">Pinjam Barang</a> / <a href="#">Kelas</a></li>
   </ol>
     <!-- <div class="container"> -->
     <div class="panel-body">
       <a href="#" class="btn btn-primary"> Kelas</a>
       <a href="frmSiswaP.php" class="btn btn-primary"> Perorangan</a>
       <br>
       <br>
        <div class="container-fluid" style="background: #FFF; padding: 10px; border-top: 3px solid #2980b9;">
         <div class="form-group">
         <label for="idpeminjaman">ID Peminjaman :</label>
         <input type="text" class="form-control" id="id">
         </div>
         <div class="form-group">
         <label for="pwd">NIS Ketua Kelas :</label>
         <input type="text" class="form-control" id="nip">
         </div>
         <div class="form-group">
         <label for="pwd">Nama :</label>
         <input type="text" class="form-control" id="nama">
         </div>
                <div class="form-group">
            <div class="col-sm-pull-3" align="left">
              <label for="pwd">Jenis Barang :</label><br>
              <select name="slct1" class="btn btn-default" id="slct1" onchange="populate(this.id,'slct2')">
                <?php 
                    $test = "SELECT * FROM tbl_jenis_barang";
                    $q = mysql_query($test); 
                     while ($data = mysql_fetch_array($q)) 
                     {
  
                ?>
                <option value="<?php echo $data ['id_jenis_barang'] ?>">
                    <?php echo $data ['nama_jenis'] ?>
              <?php } ?>
              </option>
              </select>
            </div>
          </div>
         <div class="form-group">
         <label for="pwd">Waktu Pinjam :</label>
         <input type="date" class="form-control" id="waktu">
         </div>
         <center>
           <div class="form-group">
             <div class="col-sm-pull-3" align="left">
               <label for="pwd"> Kelas :</label><br>
               <select name="slct1" class="btn btn-default" id="slct1" onchange="populate(this.id,'slct2')">
                 <option value="X RPL">X RPL</option>
                 <option value="XI RPL">XI RPL</option>
                 <option value="XII RPL">XII RPL</option>
                 <option value="X ANM">X ANM</option>
                 <option value="XI ANM">XI ANM</option>
                 <option value="XII ANM">XII ANM</option>
                 <option value="X TKR">X TKR</option>
                 <option value="XI TKR">XI TKR</option>
                 <option value="XII TKR">XII TKR</option>
                 <option value="X TP">X TP</option>
                 <option value="XI TP">XI TP</option>
               </select>
               </div>
             </div>
           </center>
           <button type="submit" class="btn btn-primary">Simpan</button>
     </div>
   </div>
         <!-- Penutup warna warni -->

     <!-- </div> -->
   </div>
 </div>


<!-- Form -->

 </body>
</html>
