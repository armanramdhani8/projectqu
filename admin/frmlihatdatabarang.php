<?php
include '../koneksi.php';
session_start();
if(!isset($_SESSION['login_user'])){
  header ('location:../admin/login.php');
}
?>

 <!DOCTYPE html>
 <html>
   <head>
     <meta charset="utf-8">
     <title>Tambah Data</title>
     <link rel="stylesheet" href="../assets/css/bootstrap.css"/>
     <link rel="stylesheet" href="../assets/css/font-awesome.css"/>
     <link rel="stylesheet" href="../assets/css/custom.css"/>
     <link href='../assets/img/icon.png' rel='shortcut icon'>
     <!-- Java Script -->
     <script type="text/javascript" src="../assets/js/jquery-2.1.4.js"></script>
     <script type="text/javascript" src="../assets/js/bootstrap.js"></script>
   </head>
   <body>
  <?php
    include 'navbar.php';
    include 'sidebar.php';
  ?>

  <div class="content" style="background-color:#ecf0f1;">
   <h2 style="margin-left:20px;"><span class="fa fa-tag" style="font-size: 30px;"></span>&nbsp;Data Barang</h2>
   <div class="col-md-12">
     <ol class="breadcrumb" style="background-color:#FAFAFA;">
     <li><a href="frmDatabarang.php">Data Barang</a> / <a href="#">Lihat Barang</a></li>
     </ol>
       <!-- <div class="container"> -->
       <div class="panel-body">
          <a href="#" class="btn btn-success btn-sm" data-toggle="modal" data-target="#myModal"><span class="fa fa-plus"></span> &nbsp;&nbsp;Tambah</a>
           <br>
           <br>
           <div class="container-fluid" style="background: #FFF; padding: 10px; border-top: 3px solid #2980b9;">
           <table class="table table-hover">
             <thead>
               <tr>
                 <th>No.</th>
                 <th>Id Barang</th>
                 <th>Id Jenis Barang</th>
                 <th>Stok Awal</th>
                 <th>Sisa Stok</th>
                 <th>Aksi</th>
               </tr>
             </thead>
             <tbody>
             <?php
               $query = "SELECT * FROM 
                        tbl_barang AS b,
                        tbl_jenis_barang AS jb 
                        WHERE 
                        b.`id_jenis_barang`=jb.`id_jenis_barang`";
               $result = mysql_query($query);
               $no = 1;
               while ($tampil = mysql_fetch_array($result)){
              ?>
               <tr>
                <td><?=$no;?></td>
                <td id="id_barang<?=$no;?>"><?php echo $tampil['id_barang']; ?></td>
                <td id="id_barang<?=$no;?>"><?php echo $tampil['nama_jenis']; ?></td>
                <td id="stok_awal<?=$no;?>"><?php echo $tampil['stok_awal']; ?></td>
                <td id="sisa_stok<?=$no;?>"><?php echo $tampil['sisa_stok']; ?></td>
                 <td>
                   <div class="btn-group">
                     <button class="tbl_ubah btn btn-warning" type="button" stok_awal="<?=$tampil['stok_awal'];?>" id_jenis_barang="<?=$tampil['id_jenis_barang'];?>" id_barang="<?=$tampil['id_barang'];?>" data-toggle="modal" data-target="#myModal2"><span class="fa fa-edit"></span>&nbsp;&nbsp;Ubah</button>
                     <a href="proses/p_hapusbarang.php?id_barang=<?=$tampil['id_barang']?>" class="btn btn-danger"><span class="fa fa-trash"></span>&nbsp;&nbsp;Hapus</a>
                   </div>
                 </td>
                 <?php
              		$no = $no +1;
              		}
              		?>
               </tr>
             </tbody>
           </table>
         </div>
       </div>
   </div>
   <script type="text/javascript">
     $(document).on('click','.tbl_ubah',function(evt){
      evt.preventDefault();
      var id_barang = $(this).attr('id_barang');
      var id_jenis_barang = $(this).attr('id_jenis_barang');
      var stok_awal = $(this).attr('stok_awal');
      var gambar = $(this).attr('gambar');
      $(document).find('#id_barang').val(id_barang);
      $(document).find('.slct1').val(id_jenis_barang);
      $(document).find('#stok_awal').val(stok_awal);
      $(document).find('#gambar').val(gambar);
     });
   </script>

  <!-- Modal -->
  <div id="myModal" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content-->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Tambah Barang</h4>
      </div>
      <div class="modal-body">
        <form class="" action="proses/p_tambahbarang.php" method="post" enctype="multipart/form-data">
          <?php
          $query = "SELECT COUNT(id_barang) AS total FROM tbl_barang";
          $select = mysql_query($query);
					$idbarang = mysql_fetch_assoc($select);

					$query = "select * from tbl_barang";
					$no = $idbarang['total'];
					$no = $no + 1;
					$idbarangFix = "";
					if($idbarang['total'] < 1){
						$idbarangFix = "BR001";
					}else if($idbarang['total'] < 10){
						$idbarangFix = "BR00".$no;
					}else if($idbarang['total'] < 100){
						$idbarangFix = "BR0".$no;
					}
					$result = mysql_query($query);
					while ($tampil = mysql_fetch_array($result)){
						?>

					<?php
						}
					?>
          <div class="form-group">
          <label for="pwd">Id Barang :</label>
          <input type="text" class="form-control" readonly="" name="id_barang" value="<?=$idbarangFix;?>">
          </div>

          <div class="form-group">
            <div class="col-sm-pull-3" align="left">
              <label for="pwd">Jenis Barang :</label><br>
              <select name="slct1" class="btn btn-default" id="slct1" onchange="populate(this.id,'slct2')">
                <?php 
                $test = "SELECT * FROM tbl_jenis_barang";
                $q = mysql_query($test); 
                while ($data = mysql_fetch_array($q)) 
                {
                ?>
                  <option value="<?php echo $data ['id_jenis_barang'] ?>"><?php echo $data ['nama_jenis'] ?>
                <?php
                }
                ?>
              </option>
              </select>
            </div>
          </div>
          <div class="form-group">
          <label for="pwd">Stok Awal :</label>
          <input type="text" class="form-control" name="stok_awal">
          </div>
          <!-- <div class="form-group">
          <label for="pwd">Sisa Stok :</label>
          <input type="text" class="form-control" name="sisa_stok">
          </div> -->
          <div class="form-group">
          <label for="pwd">Gambar :</label>
          <input type="file" class="form-control" name="gambar">
          </div>
      </div>
      <div class="modal-footer">
        <input type="submit" value="Kirim" name="kirim" class="btn btn-primary">
        </form>
      </div>
    </div>

  </div>
  </div>
  <!-- End Modal -->

  <!-- Modal Edit -->
  <div id="myModal2" class="modal fade" role="dialog">
  <div class="modal-dialog">

    <!-- Modal content -->
    <div class="modal-content">
      <div class="modal-header">
        <button type="button" class="close" data-dismiss="modal">&times;</button>
        <h4 class="modal-title">Modal Header</h4>
      </div>
      <div class="modal-body">
        <form class="" action="proses/p_ubahbarang.php" method="post" enctype="multipart/form-data">

          <div class="form-group">
          <label for="pwd">Id Barang :</label>
          <input type="text" class="form-control" name="id_barang" value="" readonly="" id="id_barang">
          </div>
          <div class="form-group">
          <label for="pwd">Jenis Barang :</label>
          <div class="form-group">
            <select name="slct1" class="btn btn-default slct1" id="slct1" onchange="populate(this.id,'slct2')">
                <?php 
                $test = "SELECT * FROM tbl_jenis_barang";
                $q = mysql_query($test); 
                while ($data = mysql_fetch_array($q)) 
                {
                ?>
                  <option value="<?php echo $data ['id_jenis_barang'] ?>"><?php echo $data ['nama_jenis'] ?>
                <?php
                }
                ?>
              </option>
              </select>
            </div>  
          </div>
          <div class="form-group">
          <label for="pwd">Stok Awal :</label>
          <input type="text" class="form-control" name="stok_awal" id="stok_awal">
          </div>
          <!-- <div class="form-group">
          <label for="pwd">Sisa Stok :</label>
          <input type="text" class="form-control" name="sisa_stok">
          </div> -->
          <div class="form-group">
          <label for="pwd">Gambar :</label>
          <input type="file" class="form-control" name="gambar2" id="gambar">
          </div>
        
      </div>
      <div class="modal-footer">
      <input type="submit" class="btn btn-primary" name="simpan" value="Ubah">
      </form>
      </div>
    </div>

  </div>
  </div>
  <!-- End Modal Edit -->
   </body>
 </html>
